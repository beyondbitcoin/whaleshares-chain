#!/usr/bin/env bash

# WLS_VERSION from env

echo "WLS_VERSION=$WLS_VERSION"

CURRENT_PATH="$(pwd)"

echo "CURRENT_PATH=$CURRENT_PATH"

if  [[ $CURRENT_PATH != */cmake-build-release ]] ;
then
    echo "exit! Pls go to cmake-build-release folder to run this script."
    exit
fi


###############################################################
# rebuild

make -j$(sysctl -n hw.logicalcpu) whaled cli_wallet


###############################################################
# whaled

cd "$CURRENT_PATH"
cd programs/whaled
echo "$(pwd)"
WHALED_ZIP="whaled-$WLS_VERSION-osx.zip"
zip "$WHALED_ZIP" whaled
cp "$WHALED_ZIP" "$CURRENT_PATH/../bin/"


###############################################################
# cli_wallet

cd "$CURRENT_PATH"
cd programs/cli_wallet
echo "$(pwd)"
CLI_WALLET_ZIP="cli_wallet-$WLS_VERSION-osx.zip"
zip "$CLI_WALLET_ZIP" cli_wallet
cp "$CLI_WALLET_ZIP" "$CURRENT_PATH/../bin/"
