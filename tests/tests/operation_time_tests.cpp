#ifdef IS_TEST_NET

#include "../common/database_fixture.hpp"

#include <wls/protocol/exceptions.hpp>
#include <wls/chain/block_summary_object.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/hardfork.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/util/reward.hpp>
#include <wls/plugins/debug_node/debug_node_plugin.hpp>

#include <fc/crypto/digest.hpp>

#include <boost/test/unit_test.hpp>

#include <cmath>

using namespace wls;
using namespace wls::chain;
using namespace wls::protocol;

BOOST_FIXTURE_TEST_SUITE(operation_time_tests, clean_database_fixture)

BOOST_AUTO_TEST_CASE(vesting_withdrawals) {
  try {
    ACTORS((alice))
    fund("alice", 100000);
    vest("alice", 100000);

    const auto &new_alice = db->get_account("alice");

    BOOST_TEST_MESSAGE("Setting up withdrawal");

    signed_transaction tx;
    withdraw_vesting_operation op;
    op.account = "alice";
    op.vesting_shares = asset(new_alice.vesting_shares.amount / 2, VESTS_SYMBOL);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    auto next_withdrawal = db->head_block_time() + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS;
    asset vesting_shares = new_alice.vesting_shares;
    asset to_withdraw = op.vesting_shares;
    asset original_vesting = vesting_shares;
    asset withdraw_rate = new_alice.vesting_withdraw_rate;

    BOOST_TEST_MESSAGE("Generating block up to first withdrawal");
    generate_blocks(next_withdrawal - (WLS_BLOCK_INTERVAL / 2), true);

    BOOST_REQUIRE(db->get_account("alice").vesting_shares.amount.value == vesting_shares.amount.value);

    BOOST_TEST_MESSAGE("Generating block to cause withdrawal");
    generate_block();

    auto fill_op = get_last_operations(1)[0].get<fill_vesting_withdraw_operation>();
    auto gpo = db->get_dynamic_global_properties();

    BOOST_REQUIRE(db->get_account("alice").vesting_shares.amount.value == (vesting_shares - withdraw_rate).amount.value);
    BOOST_REQUIRE((withdraw_rate * gpo.get_vesting_share_price()).amount.value - db->get_account("alice").balance.amount.value <=
                  1);  // Check a range due to differences in the share price
    BOOST_REQUIRE(fill_op.from_account == "alice");
    BOOST_REQUIRE(fill_op.to_account == "alice");
    BOOST_REQUIRE(fill_op.withdrawn.amount.value == withdraw_rate.amount.value);
    BOOST_REQUIRE(std::abs((fill_op.deposited - fill_op.withdrawn * gpo.get_vesting_share_price()).amount.value) <= 1);
    validate_database();

    BOOST_TEST_MESSAGE("Generating the rest of the blocks in the withdrawal");

    vesting_shares = db->get_account("alice").vesting_shares;
    auto balance = db->get_account("alice").balance;
    auto old_next_vesting = db->get_account("alice").next_vesting_withdrawal;

    for (int i = 1; i < WLS_VESTING_WITHDRAW_INTERVALS - 1; i++) {
      generate_blocks(db->head_block_time() + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS);

      const auto &alice = db->get_account("alice");

      gpo = db->get_dynamic_global_properties();
      fill_op = get_last_operations(1)[0].get<fill_vesting_withdraw_operation>();

      BOOST_REQUIRE(alice.vesting_shares.amount.value == (vesting_shares - withdraw_rate).amount.value);
      BOOST_REQUIRE(balance.amount.value + (withdraw_rate * gpo.get_vesting_share_price()).amount.value - alice.balance.amount.value <= 1);
      BOOST_REQUIRE(fill_op.from_account == "alice");
      BOOST_REQUIRE(fill_op.to_account == "alice");
      BOOST_REQUIRE(fill_op.withdrawn.amount.value == withdraw_rate.amount.value);
      BOOST_REQUIRE(std::abs((fill_op.deposited - fill_op.withdrawn * gpo.get_vesting_share_price()).amount.value) <= 1);

      if (i == WLS_VESTING_WITHDRAW_INTERVALS - 1)
        BOOST_REQUIRE(alice.next_vesting_withdrawal == fc::time_point_sec::maximum());
      else
        BOOST_REQUIRE(alice.next_vesting_withdrawal.sec_since_epoch() == (old_next_vesting + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS).sec_since_epoch());

      validate_database();

      vesting_shares = alice.vesting_shares;
      balance = alice.balance;
      old_next_vesting = alice.next_vesting_withdrawal;
    }

    if (to_withdraw.amount.value % withdraw_rate.amount.value != 0) {
      BOOST_TEST_MESSAGE("Generating one more block to take care of remainder");
      generate_blocks(db->head_block_time() + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS, true);
      fill_op = get_last_operations(1)[0].get<fill_vesting_withdraw_operation>();
      gpo = db->get_dynamic_global_properties();

      BOOST_REQUIRE(db->get_account("alice").next_vesting_withdrawal.sec_since_epoch() ==
                    (old_next_vesting + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS).sec_since_epoch());
      BOOST_REQUIRE(fill_op.from_account == "alice");
      BOOST_REQUIRE(fill_op.to_account == "alice");
      BOOST_REQUIRE(fill_op.withdrawn.amount.value == withdraw_rate.amount.value);
      BOOST_REQUIRE(std::abs((fill_op.deposited - fill_op.withdrawn * gpo.get_vesting_share_price()).amount.value) <= 1);

      generate_blocks(db->head_block_time() + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS, true);
      gpo = db->get_dynamic_global_properties();
      fill_op = get_last_operations(1)[0].get<fill_vesting_withdraw_operation>();

      BOOST_REQUIRE(db->get_account("alice").next_vesting_withdrawal.sec_since_epoch() == fc::time_point_sec::maximum().sec_since_epoch());
      BOOST_REQUIRE(fill_op.to_account == "alice");
      BOOST_REQUIRE(fill_op.from_account == "alice");
      BOOST_REQUIRE(fill_op.withdrawn.amount.value == to_withdraw.amount.value % withdraw_rate.amount.value);
      BOOST_REQUIRE(std::abs((fill_op.deposited - fill_op.withdrawn * gpo.get_vesting_share_price()).amount.value) <= 1);

      validate_database();
    } else {
      generate_blocks(db->head_block_time() + WLS_VESTING_WITHDRAW_INTERVAL_SECONDS, true);

      BOOST_REQUIRE(db->get_account("alice").next_vesting_withdrawal.sec_since_epoch() == fc::time_point_sec::maximum().sec_since_epoch());

      fill_op = get_last_operations(1)[0].get<fill_vesting_withdraw_operation>();
      BOOST_REQUIRE(fill_op.from_account == "alice");
      BOOST_REQUIRE(fill_op.to_account == "alice");
      BOOST_REQUIRE(fill_op.withdrawn.amount.value == withdraw_rate.amount.value);
      BOOST_REQUIRE(std::abs((fill_op.deposited - fill_op.withdrawn * gpo.get_vesting_share_price()).amount.value) <= 1);
    }

    BOOST_REQUIRE(db->get_account("alice").vesting_shares.amount.value == (original_vesting - op.vesting_shares).amount.value);
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(vesting_withdraw_route) {
  try {
    ACTORS((alice)(bob)(sam))

    auto original_vesting = alice.vesting_shares;

    fund("alice", 1040000);
    vest("alice", 1040000);

    auto withdraw_amount = alice.vesting_shares - original_vesting;

    BOOST_TEST_MESSAGE("Setup vesting withdraw");
    withdraw_vesting_operation wv;
    wv.account = "alice";
    wv.vesting_shares = withdraw_amount;

    signed_transaction tx;
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.operations.push_back(wv);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    tx.operations.clear();
    tx.signatures.clear();

    BOOST_TEST_MESSAGE("Setting up bob destination");
    set_withdraw_vesting_route_operation op;
    op.from_account = "alice";
    op.to_account = "bob";
    op.percent = WLS_1_PERCENT * 50;
    op.auto_vest = true;
    tx.operations.push_back(op);

    BOOST_TEST_MESSAGE("Setting up sam destination");
    op.to_account = "sam";
    op.percent = WLS_1_PERCENT * 30;
    op.auto_vest = false;
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_TEST_MESSAGE("Setting up first withdraw");

    auto vesting_withdraw_rate = alice.vesting_withdraw_rate;
    auto old_alice_balance = alice.balance;
    auto old_alice_vesting = alice.vesting_shares;
    auto old_bob_balance = bob.balance;
    auto old_bob_vesting = bob.vesting_shares;
    auto old_sam_balance = sam.balance;
    auto old_sam_vesting = sam.vesting_shares;
    generate_blocks(alice.next_vesting_withdrawal, true);

    {
      const auto &alice = db->get_account("alice");
      const auto &bob = db->get_account("bob");
      const auto &sam = db->get_account("sam");

      BOOST_REQUIRE(alice.vesting_shares == old_alice_vesting - vesting_withdraw_rate);
      BOOST_REQUIRE(alice.balance == old_alice_balance + asset((vesting_withdraw_rate.amount * WLS_1_PERCENT * 20) / WLS_100_PERCENT, VESTS_SYMBOL) *
                                                             db->get_dynamic_global_properties().get_vesting_share_price());
      BOOST_REQUIRE(bob.vesting_shares == old_bob_vesting + asset((vesting_withdraw_rate.amount * WLS_1_PERCENT * 50) / WLS_100_PERCENT, VESTS_SYMBOL));
      BOOST_REQUIRE(bob.balance == old_bob_balance);
      BOOST_REQUIRE(sam.vesting_shares == old_sam_vesting);
      BOOST_REQUIRE(sam.balance == old_sam_balance + asset((vesting_withdraw_rate.amount * WLS_1_PERCENT * 30) / WLS_100_PERCENT, VESTS_SYMBOL) *
                                                         db->get_dynamic_global_properties().get_vesting_share_price());

      old_alice_balance = alice.balance;
      old_alice_vesting = alice.vesting_shares;
      old_bob_balance = bob.balance;
      old_bob_vesting = bob.vesting_shares;
      old_sam_balance = sam.balance;
      old_sam_vesting = sam.vesting_shares;
    }

    BOOST_TEST_MESSAGE("Test failure with greater than 100% destination assignment");

    tx.operations.clear();
    tx.signatures.clear();

    op.to_account = "sam";
    op.percent = WLS_1_PERCENT * 50 + 1;
    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception);

    BOOST_TEST_MESSAGE("Test from_account receiving no withdraw");

    tx.operations.clear();
    tx.signatures.clear();

    op.to_account = "sam";
    op.percent = WLS_1_PERCENT * 50;
    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    generate_blocks(db->get_account("alice").next_vesting_withdrawal, true);
    {
      const auto &alice = db->get_account("alice");
      const auto &bob = db->get_account("bob");
      const auto &sam = db->get_account("sam");

      BOOST_REQUIRE(alice.vesting_shares == old_alice_vesting - vesting_withdraw_rate);
      BOOST_REQUIRE(alice.balance == old_alice_balance);
      BOOST_REQUIRE(bob.vesting_shares == old_bob_vesting + asset((vesting_withdraw_rate.amount * WLS_1_PERCENT * 50) / WLS_100_PERCENT, VESTS_SYMBOL));
      BOOST_REQUIRE(bob.balance == old_bob_balance);
      BOOST_REQUIRE(sam.vesting_shares == old_sam_vesting);
      BOOST_REQUIRE(sam.balance == old_sam_balance + asset((vesting_withdraw_rate.amount * WLS_1_PERCENT * 50) / WLS_100_PERCENT, VESTS_SYMBOL) *
                                                         db->get_dynamic_global_properties().get_vesting_share_price());
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(steem_inflation) {
  try {
     /*
        BOOST_TEST_MESSAGE( "Testing STEEM Inflation until the vesting start block" );

        auto gpo = db->get_dynamic_global_properties();
        auto witness_name = db.get_scheduled_witness(1);
        auto old_witness_balance = db->get_account( witness_name ).balance;
        auto old_witness_shares = db->get_account( witness_name ).vesting_shares;

        auto new_rewards = std::max( WLS_MIN_CONTENT_REWARD, asset( ( WLS_CONTENT_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) )
           + std::max( WLS_MIN_CURATE_REWARD, asset( ( WLS_CURATE_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) );
        auto witness_pay = std::max( WLS_MIN_PRODUCER_REWARD, asset( ( WLS_PRODUCER_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) );
        auto witness_pay_shares = asset( 0, VESTS_SYMBOL );
        auto new_vesting_steem = asset( 0, WLS_SYMBOL );
        auto new_vesting_shares = gpo.total_vesting_shares;

        if ( db->get_account( witness_name ).vesting_shares.amount.value == 0 )
        {
           new_vesting_steem += witness_pay;
           new_vesting_shares += witness_pay * ( gpo.total_vesting_shares / gpo.total_vesting_fund_steem );
        }

        auto new_supply = gpo.current_supply + new_rewards + witness_pay + new_vesting_steem;
        new_rewards += gpo.total_reward_fund_steem;
        new_vesting_steem += gpo.total_vesting_fund_steem;

        generate_block();

        gpo = db->get_dynamic_global_properties();

        BOOST_REQUIRE( gpo.current_supply.amount.value == new_supply.amount.value );
        BOOST_REQUIRE( gpo.total_reward_fund_steem.amount.value == new_rewards.amount.value );
        BOOST_REQUIRE( gpo.total_vesting_fund_steem.amount.value == new_vesting_steem.amount.value );
        BOOST_REQUIRE( gpo.total_vesting_shares.amount.value == new_vesting_shares.amount.value );
        BOOST_REQUIRE( db->get_account( witness_name ).balance.amount.value == ( old_witness_balance + witness_pay ).amount.value );

        validate_database();

        while( db.head_block_num() < WLS_START_VESTING_BLOCK - 1)
        {
           witness_name = db.get_scheduled_witness(1);
           old_witness_balance = db->get_account( witness_name ).balance;
           old_witness_shares = db->get_account( witness_name ).vesting_shares;


           new_rewards = std::max( WLS_MIN_CONTENT_REWARD, asset( ( WLS_CONTENT_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) )
              + std::max( WLS_MIN_CURATE_REWARD, asset( ( WLS_CURATE_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) );
           witness_pay = std::max( WLS_MIN_PRODUCER_REWARD, asset( ( WLS_PRODUCER_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) );
           new_vesting_steem = asset( 0, WLS_SYMBOL );
           new_vesting_shares = gpo.total_vesting_shares;

           if ( db->get_account( witness_name ).vesting_shares.amount.value == 0 )
           {
              new_vesting_steem += witness_pay;
              witness_pay_shares = witness_pay * gpo.get_vesting_share_price();
              new_vesting_shares += witness_pay_shares;
              new_supply += witness_pay;
              witness_pay = asset( 0, WLS_SYMBOL );
           }

           new_supply = gpo.current_supply + new_rewards + witness_pay + new_vesting_steem;
           new_rewards += gpo.total_reward_fund_steem;
           new_vesting_steem += gpo.total_vesting_fund_steem;

           generate_block();

           gpo = db->get_dynamic_global_properties();

           BOOST_REQUIRE( gpo.current_supply.amount.value == new_supply.amount.value );
           BOOST_REQUIRE( gpo.total_reward_fund_steem.amount.value == new_rewards.amount.value );
           BOOST_REQUIRE( gpo.total_vesting_fund_steem.amount.value == new_vesting_steem.amount.value );
           BOOST_REQUIRE( gpo.total_vesting_shares.amount.value == new_vesting_shares.amount.value );
           BOOST_REQUIRE( db->get_account( witness_name ).balance.amount.value == ( old_witness_balance + witness_pay ).amount.value );
           BOOST_REQUIRE( db->get_account( witness_name ).vesting_shares.amount.value == ( old_witness_shares + witness_pay_shares ).amount.value );

           validate_database();
        }

        BOOST_TEST_MESSAGE( "Testing up to the start block for miner voting" );

        while( db.head_block_num() < WLS_START_MINER_VOTING_BLOCK - 1 )
        {
           witness_name = db.get_scheduled_witness(1);
           old_witness_balance = db->get_account( witness_name ).balance;

           new_rewards = std::max( WLS_MIN_CONTENT_REWARD, asset( ( WLS_CONTENT_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) )
              + std::max( WLS_MIN_CURATE_REWARD, asset( ( WLS_CURATE_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) );
           witness_pay = std::max( WLS_MIN_PRODUCER_REWARD, asset( ( WLS_PRODUCER_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) );
           auto witness_pay_shares = asset( 0, VESTS_SYMBOL );
           new_vesting_steem = asset( ( witness_pay + new_rewards ).amount * 9, WLS_SYMBOL );
           new_vesting_shares = gpo.total_vesting_shares;

           if ( db->get_account( witness_name ).vesting_shares.amount.value == 0 )
           {
              new_vesting_steem += witness_pay;
              witness_pay_shares = witness_pay * gpo.get_vesting_share_price();
              new_vesting_shares += witness_pay_shares;
              new_supply += witness_pay;
              witness_pay = asset( 0, WLS_SYMBOL );
           }

           new_supply = gpo.current_supply + new_rewards + witness_pay + new_vesting_steem;
           new_rewards += gpo.total_reward_fund_steem;
           new_vesting_steem += gpo.total_vesting_fund_steem;

           generate_block();

           gpo = db->get_dynamic_global_properties();

           BOOST_REQUIRE( gpo.current_supply.amount.value == new_supply.amount.value );
           BOOST_REQUIRE( gpo.total_reward_fund_steem.amount.value == new_rewards.amount.value );
           BOOST_REQUIRE( gpo.total_vesting_fund_steem.amount.value == new_vesting_steem.amount.value );
           BOOST_REQUIRE( gpo.total_vesting_shares.amount.value == new_vesting_shares.amount.value );
           BOOST_REQUIRE( db->get_account( witness_name ).balance.amount.value == ( old_witness_balance + witness_pay ).amount.value );
           BOOST_REQUIRE( db->get_account( witness_name ).vesting_shares.amount.value == ( old_witness_shares + witness_pay_shares ).amount.value );

           validate_database();
        }

        for( int i = 0; i < WLS_BLOCKS_PER_DAY; i++ )
        {
           witness_name = db.get_scheduled_witness(1);
           old_witness_balance = db->get_account( witness_name ).balance;

           new_rewards = std::max( WLS_MIN_CONTENT_REWARD, asset( ( WLS_CONTENT_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) )
              + std::max( WLS_MIN_CURATE_REWARD, asset( ( WLS_CURATE_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) );
           witness_pay = std::max( WLS_MIN_PRODUCER_REWARD, asset( ( WLS_PRODUCER_APR * gpo.current_supply.amount ) / ( WLS_BLOCKS_PER_YEAR * 100 ), WLS_SYMBOL ) );
           witness_pay_shares = witness_pay * gpo.get_vesting_share_price();
           new_vesting_steem = asset( ( witness_pay + new_rewards ).amount * 9, WLS_SYMBOL ) + witness_pay;
           new_vesting_shares = gpo.total_vesting_shares + witness_pay_shares;
           new_supply = gpo.current_supply + new_rewards + new_vesting_steem;
           new_rewards += gpo.total_reward_fund_steem;
           new_vesting_steem += gpo.total_vesting_fund_steem;

           generate_block();

           gpo = db->get_dynamic_global_properties();

           BOOST_REQUIRE( gpo.current_supply.amount.value == new_supply.amount.value );
           BOOST_REQUIRE( gpo.total_reward_fund_steem.amount.value == new_rewards.amount.value );
           BOOST_REQUIRE( gpo.total_vesting_fund_steem.amount.value == new_vesting_steem.amount.value );
           BOOST_REQUIRE( gpo.total_vesting_shares.amount.value == new_vesting_shares.amount.value );
           BOOST_REQUIRE( db->get_account( witness_name ).vesting_shares.amount.value == ( old_witness_shares + witness_pay_shares ).amount.value );

           validate_database();
        }

        vesting_shares = gpo.total_vesting_shares;
        vesting_steem = gpo.total_vesting_fund_steem;
        reward_steem = gpo.total_reward_fund_steem;

        witness_name = db.get_scheduled_witness(1);
        old_witness_shares = db->get_account( witness_name ).vesting_shares;

        generate_block();

        gpo = db->get_dynamic_global_properties();

        BOOST_REQUIRE_EQUAL( gpo.total_vesting_fund_steem.amount.value,
           ( vesting_steem.amount.value
              + ( ( ( uint128_t( current_supply.amount.value ) / 10 ) / WLS_BLOCKS_PER_YEAR ) * 9 )
              + ( uint128_t( current_supply.amount.value ) / 100 / WLS_BLOCKS_PER_YEAR ) ).to_uint64() );
        BOOST_REQUIRE_EQUAL( gpo.total_reward_fund_steem.amount.value,
           reward_steem.amount.value + current_supply.amount.value / 10 / WLS_BLOCKS_PER_YEAR + current_supply.amount.value / 10 / WLS_BLOCKS_PER_DAY );
        BOOST_REQUIRE_EQUAL( db->get_account( witness_name ).vesting_shares.amount.value,
           old_witness_shares.amount.value + ( asset( ( ( current_supply.amount.value / WLS_BLOCKS_PER_YEAR ) * WLS_1_PERCENT ) / WLS_100_PERCENT, WLS_SYMBOL ) * ( vesting_shares / vesting_steem ) ).amount.value );
        validate_database();
        */
  }
  FC_LOG_AND_RETHROW();
}

BOOST_AUTO_TEST_CASE(post_rate_limit) {
  try {
    ACTORS((alice))

    fund("alice", 10000);
    vest("alice", 10000);

    social_action_operation op;
    social_action_comment_create action;

    action.permlink = "test1";
    action.parent_permlink = "test";
    action.body = "test";

    op.account = "alice";
    op.action = action;

    signed_transaction tx;

    tx.operations.push_back(op);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_REQUIRE(db->get_comment("alice", string("test1")).reward_weight == WLS_100_PERCENT);

    tx.operations.clear();
    tx.signatures.clear();

    generate_blocks(db->head_block_time() + WLS_MIN_ROOT_COMMENT_INTERVAL + fc::seconds(WLS_BLOCK_INTERVAL), true);

    action.permlink = "test2";
    op.action = action;

    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_REQUIRE(db->get_comment("alice", string("test2")).reward_weight == WLS_100_PERCENT);

    generate_blocks(db->head_block_time() + WLS_MIN_ROOT_COMMENT_INTERVAL + fc::seconds(WLS_BLOCK_INTERVAL), true);

    tx.operations.clear();
    tx.signatures.clear();

    action.permlink = "test3";
    op.action = action;

    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_REQUIRE(db->get_comment("alice", string("test3")).reward_weight == WLS_100_PERCENT);

    generate_blocks(db->head_block_time() + WLS_MIN_ROOT_COMMENT_INTERVAL + fc::seconds(WLS_BLOCK_INTERVAL), true);

    tx.operations.clear();
    tx.signatures.clear();

    action.permlink = "test4";
    op.action = action;

    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_REQUIRE(db->get_comment("alice", string("test4")).reward_weight == WLS_100_PERCENT);

    generate_blocks(db->head_block_time() + WLS_MIN_ROOT_COMMENT_INTERVAL + fc::seconds(WLS_BLOCK_INTERVAL), true);

    tx.operations.clear();
    tx.signatures.clear();

    action.permlink = "test5";
    op.action = action;

    tx.operations.push_back(op);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);

    BOOST_REQUIRE(db->get_comment("alice", string("test5")).reward_weight == WLS_100_PERCENT);
  }
  FC_LOG_AND_RETHROW()
}

/**
 * TODO: not allow to reward & reply to archived comment (archive comment is comment having its root-post no activity within 30 days)
 */
BOOST_AUTO_TEST_CASE(comment_freeze) {
//  try {
//    generate_blocks(1000);
//    ACTORS((alice)(bob)(sam)(dave))
//    generate_block();
//    fund("alice", 10000);
//    fund("bob", 10000);
//    fund("sam", 10000);
//    fund("dave", 10000);
//
//    vest("alice", 10000);
//    vest("bob", 10000);
//    vest("sam", 10000);
//    vest("dave", 10000);
//    generate_block();
//
//    signed_transaction tx;
//    social_action_operation comment;
//    social_action_comment_create action;
//
//    {
//      action.parent_author = "";
//      action.permlink = "test";
//      action.parent_permlink = "test";
//      action.body = "test";
//
//      comment.account = "alice";
//      comment.action = action;
//
//
//      tx.operations.push_back(comment);
//      tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//    }
//    {
//      social_action_comment_update comment_update;
//      comment_update.permlink = "test";
//      comment_update.body = "test2";
//      comment.action = comment_update;
//
//      tx.operations.clear();
//      tx.signatures.clear();
//
//      tx.operations.push_back(comment);
//      tx.sign(alice_private_key, db->get_chain_id());
//      db->push_transaction(tx, 0);
//    }
//
//    BOOST_REQUIRE(db->get_comment("alice", string("test")).last_payout == fc::time_point_sec::min());
//    BOOST_REQUIRE(db->get_comment("alice", string("test")).cashout_time != fc::time_point_sec::min());
//    BOOST_REQUIRE(db->get_comment("alice", string("test")).cashout_time != fc::time_point_sec::maximum());
//    generate_blocks(db->get_comment("alice", string("test")).cashout_time, true);
//    BOOST_REQUIRE(db->get_comment("alice", string("test")).last_payout == db->head_block_time());
//    BOOST_REQUIRE(db->get_comment("alice", string("test")).cashout_time == fc::time_point_sec::maximum());
//    BOOST_REQUIRE(db->get_comment("alice", string("test")).net_rshares.value == 0);
//    BOOST_REQUIRE(db->get_comment("alice", string("test")).abs_rshares.value == 0);
//
//    {
//      social_action_comment_update comment_update;
//      comment_update.permlink = "test";
//      comment_update.body ="test4";
//      comment.action = comment_update;
//
//
//      tx.operations.clear();
//      tx.signatures.clear();
//
//      tx.operations.push_back(comment);
//      tx.sign(alice_private_key, db->get_chain_id());
//      WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception)
//    }
//  }
//  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_CASE(clear_null_account) {
  try {
    BOOST_TEST_MESSAGE("Testing clearing the null account's balances on block");

    ACTORS((alice));
    generate_block();

    fund("alice", 10000); //fund("alice", ASSET("10.000 TESTS"));
    generate_block();

    transfer_operation transfer1;
    transfer1.from = "alice";
    transfer1.to = WLS_NULL_ACCOUNT;
    transfer1.amount = ASSET("1.000 TESTS");

    transfer_to_vesting_operation vest;
    vest.from = "alice";
    vest.to = WLS_NULL_ACCOUNT;
    vest.amount = ASSET("3.000 TESTS");

    BOOST_TEST_MESSAGE("--- Transferring to NULL Account");

    signed_transaction tx;
    tx.operations.push_back(transfer1);
    tx.operations.push_back(vest);
    tx.set_expiration(db->head_block_time() + WLS_MAX_TIME_UNTIL_EXPIRATION);
    tx.sign(alice_private_key, db->get_chain_id());
    db->push_transaction(tx, 0);
    validate_database();

    db_plugin->debug_update([=](database &db) {
      db.modify(db.get_account(WLS_NULL_ACCOUNT), [&](account_object &a) {
            a.reward_steem_balance = ASSET("1.000 TESTS");
            a.reward_vesting_balance = ASSET("1.000000 VESTS");
            a.reward_vesting_steem = ASSET("1.000 TESTS");
          });

      db.modify(db.get_dynamic_global_properties(), [&](dynamic_global_property_object &gpo) {
            gpo.current_supply += ASSET("2.000 TESTS");
            gpo.pending_rewarded_vesting_shares += ASSET("1.000000 VESTS");
            gpo.pending_rewarded_vesting_steem += ASSET("1.000 TESTS");
          });
        }, default_skip);

    validate_database();

    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).balance == ASSET("1.000 TESTS"));
    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).vesting_shares > ASSET("0.000000 VESTS"));
    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).reward_steem_balance == ASSET("1.000 TESTS"));
    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).reward_vesting_balance == ASSET("1.000000 VESTS"));
    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).reward_vesting_steem == ASSET("1.000 TESTS"));
    BOOST_REQUIRE(db->get_account("alice").balance == ASSET("6.000 TESTS"));

    BOOST_TEST_MESSAGE("--- Generating block to clear balances");
    generate_block();
    validate_database();

    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).balance == ASSET("0.000 TESTS"));
    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).vesting_shares == ASSET("0.000000 VESTS"));
    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).reward_steem_balance == ASSET("0.000 TESTS"));
    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).reward_vesting_balance == ASSET("0.000000 VESTS"));
    BOOST_REQUIRE(db->get_account(WLS_NULL_ACCOUNT).reward_vesting_steem == ASSET("0.000 TESTS"));
    BOOST_REQUIRE(db->get_account("alice").balance == ASSET("6.000 TESTS"));
  }
  FC_LOG_AND_RETHROW()
}

/**
 * TODO:
 * - normal test
 * - unclaimed token will be lost (move to content_reward)
 * - change account vesting_shares after cycle start time
 * - new account not allowed to claim in current cycle
 */
BOOST_AUTO_TEST_CASE(daily_vesting_reward) {
  try {
    BOOST_TEST_MESSAGE("Testing daily vesting reward");

    generate_block();
    ACTORS((alice))
    fund("alice", 2000000);  // 2'000 WLS
    vest("alice", 1000000);

    auto claim = [&](bool half) {
      signed_transaction tx;
      social_action_operation op;
      social_action_claim_vesting_reward action;

      {  // figure out how much user could claim
        const auto &new_alice = db->get_account("alice");
        const auto &cprops = db->get_dynamic_global_properties();

        int64_t account_vshares = 0;
        if (new_alice.vr_snaphost_next_cycle < cprops.vr_cycle) account_vshares = new_alice.vesting_shares.amount.value;
        else if (new_alice.vr_snaphost_cycle < cprops.vr_cycle) account_vshares = new_alice.vr_snaphost_shares.amount.value;

        uint128_t vest(account_vshares);  // vest(new_alice.vr_snaphost_shares.amount.value);
        uint128_t total_vest(cprops.vr_snaphost_shares.amount.value);
        uint128_t fund(cprops.vr_snaphost_payout.amount.value);
        uint128_t max_uint128 = vest * fund / total_vest;
        const auto max = max_uint128.to_uint64();
        const auto remain = (new_alice.vr_claimed_cycle == cprops.vr_cycle) ? int64_t(max) - new_alice.vr_claimed_amount.amount.value : int64_t(max);

        if (half) action.amount = asset(remain/2, WLS_SYMBOL);
        else action.amount = asset(remain, WLS_SYMBOL);
      }

      if (action.amount.amount.value == 0) {
        ilog("nothing to claim!");
        return;
      }

      action.validate();
      op.account = "alice";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(alice_private_key, db->get_chain_id());
      db->push_transaction(tx, 0);
      validate_database();
      generate_block();
    };

    auto print_debug = [&](const string& str, const string& accname) {
      const auto& dbg_acc = db->get_account(accname);
      ilog(accname + " " + str +
      ": vesting_shares=" + dbg_acc.vesting_shares.to_string() +
      ", vr_snaphost_next_cycle=" + std::to_string(dbg_acc.vr_snaphost_next_cycle) +
      ", vr_snaphost_shares=" + dbg_acc.vr_snaphost_shares.to_string() +
      ", vr_snaphost_cycle=" + std::to_string(dbg_acc.vr_snaphost_cycle) +
      ", vr_claimed_cycle=" + std::to_string(dbg_acc.vr_claimed_cycle) +
      ", vr_claimed_amount=" + dbg_acc.vr_claimed_amount.to_string());
    };

    print_debug("after init", "alice");
    generate_blocks(WLS_VESTING_REWARD_BLOCKS_PER_CYCLE - (db->get_dynamic_global_properties().head_block_number % WLS_VESTING_REWARD_BLOCKS_PER_CYCLE));
    print_debug("end of new cycle", "alice");

    claim(true);
    print_debug("after claimed half", "alice");

    claim(false);
    print_debug("after claimed remain", "alice");

    vest("alice", 1);
    print_debug("after vest", "alice");

    generate_blocks(WLS_VESTING_REWARD_BLOCKS_PER_CYCLE - (db->get_dynamic_global_properties().head_block_number % WLS_VESTING_REWARD_BLOCKS_PER_CYCLE));
    claim(false);
    print_debug("after claimed full in new cycle", "alice");

    {
      BOOST_TEST_MESSAGE("--- Test failure on claiming of new account in current cycle");

      ACTORS((bob))
      fund("bob", 1000000);  // 1'000 WLS
      vest("bob", 1000000);
      generate_block();

      print_debug("bob: ", "bob");

      signed_transaction tx;
      social_action_operation op;
      social_action_claim_vesting_reward action;

      // figure out how much user could claim
      const auto &new_acc = db->get_account("bob");
      const auto &cprops = db->get_dynamic_global_properties();

      int64_t account_vshares = 0;
      if (new_acc.vr_snaphost_next_cycle < cprops.vr_cycle) account_vshares = new_acc.vesting_shares.amount.value;
      else if (new_acc.vr_snaphost_cycle < cprops.vr_cycle) account_vshares = new_acc.vr_snaphost_shares.amount.value;

      uint128_t vest(account_vshares);
      uint128_t total_vest(cprops.vr_snaphost_shares.amount.value);
      uint128_t fund(cprops.vr_snaphost_payout.amount.value);
      uint128_t max_uint128 = vest * fund / total_vest;
      const auto max = max_uint128.to_uint64();
      const auto remain = (new_acc.vr_claimed_cycle == cprops.vr_cycle) ? int64_t(max) - new_acc.vr_claimed_amount.amount.value : int64_t(max);

      action.amount = asset(remain, WLS_SYMBOL);
      WLS_REQUIRE_THROW(action.validate(), fc::assert_exception);
    }

    {
      BOOST_TEST_MESSAGE("--- Test success on claiming of new account in next cycle");

      ACTORS((charlie))
      fund("charlie", 1000000);
      vest("charlie", 1000000);
      generate_block();

      generate_blocks(WLS_VESTING_REWARD_BLOCKS_PER_CYCLE - (db->get_dynamic_global_properties().head_block_number % WLS_VESTING_REWARD_BLOCKS_PER_CYCLE));

      print_debug("charlie: ", "charlie");

      signed_transaction tx;
      social_action_operation op;
      social_action_claim_vesting_reward action;

      // figure out how much user could claim
      const auto &new_acc = db->get_account("charlie");
      const auto &cprops = db->get_dynamic_global_properties();

      int64_t account_vshares = 0;
      if (new_acc.vr_snaphost_next_cycle < cprops.vr_cycle)
        account_vshares = new_acc.vesting_shares.amount.value;
      else if (new_acc.vr_snaphost_cycle < cprops.vr_cycle)
        account_vshares = new_acc.vr_snaphost_shares.amount.value;

      uint128_t vest(account_vshares);
      uint128_t total_vest(cprops.vr_snaphost_shares.amount.value);
      uint128_t fund(cprops.vr_snaphost_payout.amount.value);
      uint128_t max_uint128 = vest * fund / total_vest;
      const auto max = max_uint128.to_uint64();
      const auto remain = (new_acc.vr_claimed_cycle == cprops.vr_cycle) ? int64_t(max) - new_acc.vr_claimed_amount.amount.value : int64_t(max);

      action.amount = asset(remain, WLS_SYMBOL);
      action.validate(); // WLS_REQUIRE_THROW(action.validate(), fc::assert_exception);
      op.account = "charlie";
      op.action = action;
      op.validate();

      tx.operations.push_back(op);
      tx.set_expiration(db->head_block_time() + WLS_MIN_TRANSACTION_EXPIRATION_LIMIT);
      tx.sign(charlie_private_key, db->get_chain_id());
      db->push_transaction(tx, 0); // WLS_REQUIRE_THROW(db->push_transaction(tx, 0), fc::exception)
      validate_database();
    }
  }
  FC_LOG_AND_RETHROW()
}

BOOST_AUTO_TEST_SUITE_END()
#endif
