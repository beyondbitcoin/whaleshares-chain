add_executable(whaled main.cpp)
if (UNIX AND NOT APPLE)
    set(rt_library rt)
endif ()

find_package(Gperftools QUIET)
if (GPERFTOOLS_FOUND)
    message(STATUS "Found gperftools; compiling whaled with TCMalloc")
    list(APPEND PLATFORM_SPECIFIC_LIBS tcmalloc)
endif ()

#if (CHANGELOG_PLUGIN)
#    SET(CHANGELOG_PLUGIN_LIBRARIES ${ROCKSDB_LIBRARY} ${ZSTD_LIBRARIES} ${SNAPPY_LIBRARIES} ${LZ4_LIBRARY})
#endif ()

SET(WLS_PLUGINS_LIBS
        acc_hist_nudb_plugin
        account_by_key_plugin
        account_by_key_api_plugin
        account_history_plugin
        block_history_plugin
        block_missed_plugin
        blogfeed_plugin
        chain_plugin
        database_api_plugin
        debug_node_plugin
        follow_plugin
        follow_api_plugin
        json_rpc_plugin
        myfeed_plugin
        network_broadcast_api_plugin
        p2p_plugin
        podfeed_plugin
        pod_announcement_plugin
        podtags_plugin
        account_tags_plugin
        sharesfeed_plugin
        tags_plugin
        tips_sent_plugin
        tips_received_plugin
        webserver_plugin
        witness_plugin
        snapshot_plugin
        notification_plugin
        )

if (WLS_STATIC_BUILD)
    target_link_libraries(whaled PRIVATE
            "-static-libstdc++ -static-libgcc"
            appbase
            wls_protocol
            wls_utilities
            fc
            ${WLS_PLUGINS_LIBS}
            ${CMAKE_DL_LIBS}
            ${PLATFORM_SPECIFIC_LIBS}
            )
else (WLS_STATIC_BUILD)
    target_link_libraries(whaled PRIVATE
            appbase
            wls_protocol
            wls_utilities
            fc
            ${WLS_PLUGINS_LIBS}
            ${CMAKE_DL_LIBS}
            ${PLATFORM_SPECIFIC_LIBS}
            )
endif (WLS_STATIC_BUILD)

install(TARGETS
        whaled

        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        )
