#include <wls/plugins/block_missed/block_missed_plugin.hpp>
#include <wls/plugins/block_missed/block_missed_objects.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace block_missed {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;

namespace detail {

class block_missed_plugin_impl {
 public:
  block_missed_plugin_impl(block_missed_plugin &_plugin, const string &path)
      : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~block_missed_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  /**
   * high level insert which appends item to feed and updates head state
   * @param item
   */
  void insert(const block_missed_object &item);

  const block_missed_state_head_object* get_block_missed_state_head_object();

  //--

  database &_db;
  block_missed_plugin &_self;

  nudbstore<block_missed_key, block_missed_object> _nudbstore_database;
  boost::signals2::connection _irreversible_block_con;

  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

const block_missed_state_head_object *block_missed_plugin_impl::get_block_missed_state_head_object() {
  try {
    return _db.find<block_missed_state_head_object>();
  }
  FC_CAPTURE_AND_RETHROW()
}

void block_missed_plugin_impl::insert(const block_missed_object &item) {
  auto head = this->get_block_missed_state_head_object();
  if (head == nullptr) {
    head = &_db.create<block_missed_state_head_object>([&](block_missed_state_head_object &obj) { });
  }

  // insert
  block_missed_key new_key{head->sequence};
  // ilog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item));
  boost::system::error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to account head state
  _db.modify(*head, [&](block_missed_state_head_object& obj) { obj.sequence++; });
}

void block_missed_plugin_impl::on_irreversible_block(uint32_t block_num) {
  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for block_missed_plugin");

  try {
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
      operation op = fc::raw::unpack<operation>(item.serialized_op);
      switch (op.which()) {
        case operation::tag<witness_block_missed_operation>::value: {
          auto wbmo = op.get<witness_block_missed_operation>();

          block_missed_object bmo;
          {
            bmo.owner = wbmo.owner;
            bmo.block_num = wbmo.block_num;
            bmo.timestamp = wbmo.timestamp;
          }

          this->insert(bmo);
        } break;
        default:
          break;
      }
    }
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

block_missed_plugin::block_missed_plugin() {}

block_missed_plugin::~block_missed_plugin() {}

void block_missed_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options()(
//  );
  // clang-format on
  cfg.add(cli);
}

void block_missed_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing block_missed plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/block_missed/";
    my = std::make_unique<detail::block_missed_plugin_impl>(*this, path);

    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

    wls::chain::add_plugin_index<block_missed_state_head_object_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void block_missed_plugin::plugin_startup() {
  try {
    ilog("block_missed_plugin: plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void block_missed_plugin::plugin_shutdown() {
  try {
    ilog("block_missed::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore<block_missed_key, block_missed_object>& block_missed_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

const block_missed_state_head_object *block_missed_plugin::get_block_missed_state_head_object() {
  return my->get_block_missed_state_head_object();
}

}  // namespace block_missed
}  // namespace plugins
}  // namespace wls
