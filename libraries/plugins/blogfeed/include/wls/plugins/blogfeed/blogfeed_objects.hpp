#pragma once

#include <wls/chain/wls_object_types.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace blogfeed {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

class blogfeed_key {
 public:
  blogfeed_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  blogfeed_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

class blogfeed_object {
 public:
  blogfeed_object() {}

  blogfeed_object(const account_name_type &_author, const std::string &permlink) : author(_author), permlink(permlink) {}

  account_name_type author;
  std::string permlink;

  std::set<std::string> shared_by;
};


//------------------------------------------------------------------------------

#ifndef BLOGFEED_SPACE_ID
#define BLOGFEED_SPACE_ID 17
#endif

enum blogfeed_object_types {
  blogfeed_state_object_type = (BLOGFEED_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class blogfeed_state_head_object : public object<blogfeed_state_object_type, blogfeed_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  blogfeed_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  account_name_type account;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef blogfeed_state_head_object::id_type blogfeed_state_head_object_id_type;

// clang-format off
struct by_account;
typedef multi_index_container<
    blogfeed_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< blogfeed_state_head_object, blogfeed_state_head_object_id_type, &blogfeed_state_head_object::id > >,
        ordered_unique< tag< by_account >, member< blogfeed_state_head_object, account_name_type, &blogfeed_state_head_object::account > >
    >,
    allocator< blogfeed_state_head_object >
> blogfeed_state_head_object_index;
// clang-format on


}  // namespace blogfeed
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::blogfeed::blogfeed_key, (account)(sequence))
FC_REFLECT(wls::plugins::blogfeed::blogfeed_object, (author)(permlink)(shared_by))
FC_REFLECT(wls::plugins::blogfeed::blogfeed_state_head_object, (id)(account)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::blogfeed::blogfeed_state_head_object, wls::plugins::blogfeed::blogfeed_state_head_object_index)
