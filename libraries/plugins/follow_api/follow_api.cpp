#include <wls/plugins/follow_api/follow_api_plugin.hpp>
#include <wls/plugins/follow_api/follow_api.hpp>

#include <wls/plugins/follow/follow_objects.hpp>

namespace wls {
namespace plugins {
namespace follow {

namespace detail {

inline void set_what(vector<follow_type> &what, uint16_t bitmask) {
  if (bitmask & 1 << blog) what.push_back(blog);
  if (bitmask & 1 << ignore) what.push_back(ignore);
}

class follow_api_impl {
 public:
  follow_api_impl() : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()) {}
  // clang-format off
  DECLARE_API_IMPL(
    (get_followers)
    (get_following)
    (get_follow_count)
    (get_feed_entries)
    (get_feed)
    (get_blog_entries)
    (get_blog)
//  (get_account_reputations)
    (get_reblogged_by)
//    (get_blog_authors)
   )
  // clang-format on
  chain::database &_db;
};

DEFINE_API_IMPL(follow_api_impl, get_followers) {
  FC_ASSERT(args.size() == 4, "Expected 4 arguments, was ${n}", ("n", args.size()));
  account_name_type account = args[0].as<account_name_type>();
  account_name_type start = args[1].as<account_name_type>();
  follow::follow_type type = args[2].as<follow::follow_type>();
  uint32_t limit = args[3].as<uint32_t>();

  FC_ASSERT(limit <= 1000);

  get_followers_return result;
  result.reserve(limit);

  const auto &idx = _db.get_index<follow::follow_index>().indices().get<follow::by_following_follower>();
  auto itr = idx.lower_bound(std::make_tuple(account, start));
  while (itr != idx.end() && result.size() < limit && itr->following == account) {
    if (type == follow::undefined || itr->what & (1 << type)) {
      api_follow_object entry;
      entry.follower = itr->follower;
      entry.following = itr->following;
      set_what(entry.what, itr->what);
      result.push_back(entry);
    }

    ++itr;
  }

  return result;
}

DEFINE_API_IMPL(follow_api_impl, get_following) {
  FC_ASSERT(args.size() == 4, "Expected 4 arguments, was ${n}", ("n", args.size()));
  account_name_type account = args[0].as<account_name_type>();
  account_name_type start = args[1].as<account_name_type>();
  follow::follow_type type = args[2].as<follow::follow_type>();
  uint32_t limit = args[3].as<uint32_t>();

  FC_ASSERT(limit <= 1000);

  vector<api_follow_object> result;
  result.reserve(limit);

  const auto &idx = _db.get_index<follow::follow_index>().indices().get<follow::by_follower_following>();
  auto itr = idx.lower_bound(std::make_tuple(account, start));
  while ((itr != idx.end()) && (result.size() < limit) && (itr->follower == account)) {
    if (type == follow::undefined || itr->what & (1 << type)) {
      api_follow_object entry;
      entry.follower = itr->follower;
      entry.following = itr->following;
      set_what(entry.what, itr->what);
      result.push_back(entry);
    }

    ++itr;
  }

  return result;
}

DEFINE_API_IMPL(follow_api_impl, get_follow_count) {
  FC_ASSERT(args.size() == 1, "Expected 1 arguments, was ${n}", ("n", args.size()));
  account_name_type account = args[0].as<account_name_type>();

  get_follow_count_return result;
  auto itr = _db.find<follow::follow_count_object, follow::by_account>(account);

  if (itr != nullptr)
    result = get_follow_count_return(*itr);
  else
    result.account = account;

  return result;
}

DEFINE_API_IMPL(follow_api_impl, get_feed_entries) {
  FC_ASSERT(args.size() == 3, "Expected 3 arguments, was ${n}", ("n", args.size()));
  account_name_type account = args[0].as<account_name_type>();
  uint32_t start_entry_id = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();
  FC_ASSERT(limit <= 500, "Cannot retrieve more than 500 feed entries at a time.");

  auto entry_id = start_entry_id == 0 ? ~0 : start_entry_id;

  vector<feed_entry> result;
  result.reserve(limit);

  const auto &feed_idx = _db.get_index<follow::feed_index>().indices().get<follow::by_feed>();
  auto itr = feed_idx.lower_bound(boost::make_tuple(account, entry_id));

  while ((itr != feed_idx.end()) && (itr->account == account) && (result.size() < limit)) {
    const auto &comment = _db.get(itr->comment);
    feed_entry entry;
    entry.author = comment.author;
    entry.permlink = chain::to_string(comment.permlink);
    entry.entry_id = itr->account_feed_id;

    if (itr->first_reblogged_by != WLS_ROOT_POST_PARENT) {
      entry.reblog_by.reserve(itr->reblogged_by.size());

      for (const auto &a : itr->reblogged_by) entry.reblog_by.push_back(a);

      entry.reblog_on = itr->first_reblogged_on;
    }

    result.push_back(entry);
    ++itr;
  }

  return result;
}

DEFINE_API_IMPL(follow_api_impl, get_feed) {
  FC_ASSERT(args.size() == 3, "Expected 3 arguments, was ${n}", ("n", args.size()));
  account_name_type account = args[0].as<account_name_type>();
  uint32_t start_entry_id = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 500, "Cannot retrieve more than 500 feed entries at a time.");

  auto entry_id = start_entry_id == 0 ? ~0 : start_entry_id;

  vector<comment_feed_entry> result;
  result.reserve(limit);

  const auto &feed_idx = _db.get_index<follow::feed_index>().indices().get<follow::by_feed>();
  auto itr = feed_idx.lower_bound(boost::make_tuple(account, entry_id));

  while ((itr != feed_idx.end()) && (itr->account == account) && (result.size() < limit)) {
    const auto &comment = _db.get(itr->comment);
    comment_feed_entry entry;
    //                  entry.comment = database_api::comment_api_obj(comment);
    entry.comment = comment;
    entry.entry_id = itr->account_feed_id;

    if (itr->first_reblogged_by != WLS_ROOT_POST_PARENT) {
      entry.reblog_by.reserve(itr->reblogged_by.size());

      for (const auto &a : itr->reblogged_by) entry.reblog_by.push_back(a);

      entry.reblog_on = itr->first_reblogged_on;
    }

    result.push_back(entry);
    ++itr;
  }

  return result;
}

DEFINE_API_IMPL(follow_api_impl, get_blog_entries) {
  FC_ASSERT(args.size() == 3, "Expected 3 arguments, was ${n}", ("n", args.size()));
  account_name_type account = args[0].as<account_name_type>();
  uint32_t start_entry_id = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 500, "Cannot retrieve more than 500 blog entries at a time.");

  auto entry_id = start_entry_id == 0 ? ~0 : start_entry_id;

  vector<blog_entry> result;
  result.reserve(limit);

  const auto &blog_idx = _db.get_index<follow::blog_index>().indices().get<follow::by_blog>();
  auto itr = blog_idx.lower_bound(boost::make_tuple(account, entry_id));

  while ((itr != blog_idx.end()) && (itr->account == account) && (result.size() < limit)) {
    const auto &comment = _db.get(itr->comment);
    blog_entry entry;
    entry.author = comment.author;
    entry.permlink = chain::to_string(comment.permlink);
    entry.blog = account;
    entry.reblog_on = itr->reblogged_on;
    entry.entry_id = itr->blog_feed_id;

    result.push_back(entry);
    ++itr;
  }

  return result;
}

DEFINE_API_IMPL(follow_api_impl, get_blog) {
  FC_ASSERT(args.size() == 3, "Expected 3 arguments, was ${n}", ("n", args.size()));
  account_name_type account = args[0].as<account_name_type>();
  uint32_t start_entry_id = args[1].as<uint32_t>();
  uint32_t limit = args[2].as<uint32_t>();

  FC_ASSERT(limit <= 500, "Cannot retrieve more than 500 blog entries at a time.");

  auto entry_id = start_entry_id == 0 ? ~0 : start_entry_id;

  vector<comment_blog_entry> result;
  result.reserve(limit);

  const auto &blog_idx = _db.get_index<follow::blog_index>().indices().get<follow::by_blog>();
  auto itr = blog_idx.lower_bound(boost::make_tuple(account, entry_id));

  while ((itr != blog_idx.end()) && (itr->account == account) && (result.size() < limit)) {
    const auto &comment = _db.get(itr->comment);
    comment_blog_entry entry;
    //                  entry.comment = database_api::api_comment_object(comment, _db);
    entry.comment = database_api::comment_api_obj(comment);
    entry.blog = account;
    entry.reblog_on = itr->reblogged_on;
    entry.entry_id = itr->blog_feed_id;

    result.push_back(entry);
    ++itr;
  }

  return result;
}

DEFINE_API_IMPL(follow_api_impl, get_reblogged_by) {
  FC_ASSERT(args.size() == 2, "Expected 2 arguments, was ${n}", ("n", args.size()));
  account_name_type author = args[0].as<account_name_type>();
  string permlink = args[0].as<string>();

  vector<account_name_type> result;

  const auto &post = _db.get_comment(author, permlink);
  const auto &blog_idx = _db.get_index<follow::blog_index, follow::by_comment>();
  auto itr = blog_idx.lower_bound(post.id);

  while ((itr != blog_idx.end()) && (itr->comment == post.id) && (result.size() < 2000)) {
    result.push_back(itr->account);
    ++itr;
  }

  return result;
}

}  // namespace detail

follow_api::follow_api() : my(new detail::follow_api_impl()) { JSON_RPC_REGISTER_API(WLS_FOLLOW_API_PLUGIN_NAME); }

follow_api::~follow_api() {}
// clang-format off
DEFINE_READ_APIS( follow_api,
  (get_followers)
  (get_following)
  (get_follow_count)
  (get_feed_entries)
  (get_feed)
  (get_blog_entries)
  (get_blog)
// (get_account_reputations)
  (get_reblogged_by)
//  (get_blog_authors)
)
// clang-format on
}  // namespace follow
}  // namespace plugins
}  // namespace wls