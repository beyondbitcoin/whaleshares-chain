#include <wls/plugins/account_history/account_history_plugin.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/protocol/config.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace account_history {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;
using chain::operation_object;

namespace detail {

class account_history_plugin_impl {
 public:
  account_history_plugin_impl(account_history_plugin &_plugin, const string &path) : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~account_history_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  /**
   * high level insert which appends item to history and updates head state
   * @param account
   * @param item
   */
  void insert(const account_name_type &account, const account_history_object &item);

  const account_history_state_head_object* get_account_history_state_head_object(const account_name_type &account);

  database &_db;
  account_history_plugin &_self;

  nudbstore<account_history_key, account_history_object> _nudbstore_database;
  boost::signals2::connection _irreversible_block_con;

  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

const account_history_state_head_object *account_history_plugin_impl::get_account_history_state_head_object(const account_name_type &account) {
  try {
    return _db.find<account_history_state_head_object, by_account>(account);
  }
  FC_CAPTURE_AND_RETHROW((account))
}

void account_history_plugin_impl::insert(const account_name_type &account, const account_history_object &item) {
  auto head = this->get_account_history_state_head_object(account);
  if (head == nullptr) {
    head = &_db.create<account_history_state_head_object>([&](account_history_state_head_object &obj) { obj.account = account; });
  }

  // insert
  account_history_key new_key{account, head->sequence};
//  ilog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item));
  boost::system::error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to account head state
  _db.modify(*head, [&](account_history_state_head_object& obj) { obj.sequence++; });
}

void account_history_plugin_impl::on_irreversible_block(uint32_t block_num) {
  /**
   * Work in this function:
   * - detect new operation
   * - put new item into the history
   */
  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for account_history_plugin");

  try {
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
//      operation op = fc::raw::unpack<operation>(item.serialized_op);
      account_history_object temp;
      {
        temp.trx_id = item.trx_id;
        temp.block = item.block;
        temp.trx_in_block = item.trx_in_block;
        temp.op_in_trx = item.op_in_trx;
        temp.virtual_op = item.virtual_op;
        temp.timestamp = item.timestamp;
        temp.op = fc::raw::unpack<operation>(item.serialized_op);
      }

      flat_set<account_name_type> impacted;
      wls::app::operation_get_impacted_accounts(temp.op, impacted);

      for (auto &impacted_acc : impacted) {
        this->insert(impacted_acc, temp);
      }
    }
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

account_history_plugin::account_history_plugin() {}

account_history_plugin::~account_history_plugin() {}

void account_history_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options();
  // clang-format on
  cfg.add(cli);
}

void account_history_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing account_history plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/account_history/";
    my = std::make_unique<detail::account_history_plugin_impl>(*this, path);

    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

    wls::chain::add_plugin_index<account_history_state_head_object_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void account_history_plugin::plugin_startup() { ilog("account_history plugin: plugin_startup()"); }

void account_history_plugin::plugin_shutdown() {
  try {
    ilog("account_history::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore<account_history_key, account_history_object>& account_history_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

const account_history_state_head_object *account_history_plugin::get_account_history_state_head_object(const account_name_type &account) {
  return my->get_account_history_state_head_object(account);
}

}  // namespace account_history
}  // namespace plugins
}  // namespace wls
