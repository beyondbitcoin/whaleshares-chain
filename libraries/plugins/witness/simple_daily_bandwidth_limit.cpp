#include <wls/plugins/witness/simple_daily_bandwidth_limit.hpp>
#include <wls/chain/database_exceptions.hpp>

namespace wls {
namespace plugins {
namespace witness {

int64_t stakeRanges[] = {
    1000,           // 0: 1
    10000,          // 1: 10
    100000,         // 2: 100
    1000000,        // 3: 1K
    10000000,       // 4: 10K
    100000000,      // 5: 100K
    1000000000,     // 6: 1M
    10000000000,    // 7: 10M
    100000000000,   // 8: 100M
    1000000000000,  // 9: 1B
};

/**
 *
 * range (whalestake) daily_bandwidth (KB)
 * 10 (> 1B    WLS): 1024
 *  9 (> 100M  WLS): 512
 *  8 (> 10    WLS): 256
 *  7 (> 1M    WLS): 128
 *  6 (> 100K  WLS): 64
 *  5 (> 10K   WLS): 32
 *  4 (> 1K    WLS): 16
 *  3 (> 100   WLS): 8
 *  2 (> 10    WLS): 4
 *  1 (> 1     WLS): 2
 *  0 (< 1     WLS): 1
 *
 * @param stake
 * @param has_hf3 if testnet or has HF3
 * @return
 */
int64_t simple_daily_bandwidth_limit(int64_t stake, bool has_hf3) {
  int64_t range = 0;

  if (stake >= stakeRanges[8]) {         // 100M
    range = 9;
  } else if (stake >= stakeRanges[7]) {  // 10M
    range = 8;
  } else if (stake >= stakeRanges[6]) {  // 1M
    range = 7;
  } else if (stake >= stakeRanges[5]) {  // 100K
    range = 6;
  } else if (stake >= stakeRanges[4]) {  // 10K
    range = 5;
  } else if (stake >= stakeRanges[3]) {  // 1K
    range = 4;
  } else if (stake >= stakeRanges[2]) {  // 100
    range = 3;
  } else if (stake >= stakeRanges[1]) {  // 10
    range = 2;
  } else if (stake >= stakeRanges[0]) {  // 1
    range = 1;
  } else {
    range = 0;
  }

  // return min/max if range = 0 or 9 here
  if (range >= 9) {
    return 512*1024;
  } else if (range <= 0) {
    return 1024; // make sure everyone has a minimum daily bandwidth
  }

  int64_t max_threshold = 1024 * (uint16_t(1) << (range + 1));
  int64_t min_threshold = max_threshold/2;
  int64_t bw_range = max_threshold - min_threshold;

  if (has_hf3) {
    int64_t max_stake = stakeRanges[range];
    int64_t min_stake = stakeRanges[range - 1];
    int64_t stake_range = max_stake - min_stake;
    //  ilog("max_threshold=${max_threshold}, min_threshold=${min_threshold}, bw_range=${bw_range}, stake_range=${stake_range}",
    //       ("max_threshold", max_threshold)("min_threshold", min_threshold)("bw_range", bw_range)("stake_range", stake_range));
    WLS_ASSERT(stake_range > 0, wls::chain::plugin_exception, "stake_range must > 0");
    int64_t daily_limit = ((stake - min_stake) * bw_range / stake_range) + min_threshold;

    return daily_limit;
  } else {
    int64_t max_stake = stakeRanges[range + 1];
    int64_t min_stake = stakeRanges[range];
    int64_t stake_range = max_stake - min_stake;
    //  ilog("max_threshold=${max_threshold}, min_threshold=${min_threshold}, bw_range=${bw_range}, stake_range=${stake_range}",
    //       ("max_threshold", max_threshold)("min_threshold", min_threshold)("bw_range", bw_range)("stake_range", stake_range));
    WLS_ASSERT(stake_range > 0, wls::chain::plugin_exception, "stake_range must > 0");
    int64_t daily_limit = ((stake - stakeRanges[range]) * bw_range / stake_range) + min_threshold;

    return daily_limit;
  }
}


}  // namespace witness
}  // namespace plugins
}  // namespace wls
