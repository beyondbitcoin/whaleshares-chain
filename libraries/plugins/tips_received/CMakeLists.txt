file(GLOB HEADERS "include/wls/plugins/tips_received/*.hpp")

add_library(tips_received_plugin
        tips_received_plugin.cpp
        )

target_link_libraries( tips_received_plugin block_history_plugin chain_plugin wls_chain wls_protocol wls_utilities )
target_include_directories(tips_received_plugin PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/include"
        "${CMAKE_CURRENT_SOURCE_DIR}/../../nudbstore/include"
        "${CMAKE_CURRENT_SOURCE_DIR}/../../vendor/NuDB/include"
        )

install(TARGETS
        tips_received_plugin

        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        )
