#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/tips_received/tips_received_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define TIPS_RECEIVED_PLUGIN_NAME "tips_received"

namespace wls {
namespace plugins {
namespace tips_received {

namespace detail {
class tips_received_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

class tips_received_plugin : public plugin<tips_received_plugin> {
 public:
  tips_received_plugin();

  virtual ~tips_received_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin))

  static const std::string &name() {
    static std::string name = TIPS_RECEIVED_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  const tips_received_state_head_object* get_tips_received_state_head_object(const account_name_type &account);

  nudbstore<tips_received_key, tips_received_object>& get_nudbstore_database();

  // friend class detail::tips_received_plugin_impl;

 private:
  std::unique_ptr<detail::tips_received_plugin_impl> my;
};

}  // namespace tips_received
}  // namespace plugins
}  // namespace wls
