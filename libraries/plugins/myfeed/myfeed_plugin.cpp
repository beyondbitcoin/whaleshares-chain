#include <wls/plugins/myfeed/myfeed_plugin.hpp>
#include <wls/plugins/myfeed/myfeed_objects.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/plugins/follow/follow_objects.hpp>
#include <wls/chain/wls_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/history_object.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/chain/util/signal.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace myfeed {

using namespace wls::protocol;

using chain::database;
using chain::operation_notification;

namespace detail {

class myfeed_plugin_impl {
 public:
  myfeed_plugin_impl(myfeed_plugin &_plugin, const string &path)
      : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin), _nudbstore_database{path} {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~myfeed_plugin_impl() {}

  void on_irreversible_block( uint32_t block_num );

  /**
   * this shit is super slow!
   */
  void fanout(const account_name_type &account, const comment_object &comment);

  /**
   * includes:
   * - friends
   * - accounts on the same pods
   * - followers
   */
  std::set<account_name_type> find_audiences(const account_name_type &account, const pod_name_type &pod);

  /**
   * high level insert which appends item to feed and updates head state
   * @param account
   * @param item
   */
  void insert(const account_name_type &account, const myfeed_object &item);

  const myfeed_state_head_object* get_myfeed_state_head_object(const account_name_type &account);

  //--

  database &_db;
  myfeed_plugin &_self;

  nudbstore<myfeed_key, myfeed_object> _nudbstore_database;
  boost::signals2::connection _irreversible_block_con;

  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

std::set<account_name_type> myfeed_plugin_impl::find_audiences(const account_name_type &account, const pod_name_type &pod) {
  set<account_name_type> result;

  { // get all friends
    const auto &idx = _db.get_index<friend_index>().indices().get<by_account_another>();
    auto itr = idx.lower_bound(std::make_tuple(account, ""));
    while (true) {
      if (itr == idx.end()) break;
      if (itr->account != account) break;
      result.insert(itr->another);
      ++itr;
    }
  }

  {  // get all followers
    const auto &idx = _db.get_index<follow::follow_index>().indices().get<follow::by_following_follower>();
    auto itr = idx.lower_bound(std::make_tuple(account, ""));
    while (itr != idx.end() && itr->following == account) {
      if (itr->what & (1 << follow::follow_type::blog)) {
        result.insert(itr->follower);
      }
      ++itr;
    }
  }

  {  // get all member on the same pod
    if (pod != "") {
      const auto &idx = _db.get_index<pod_member_index>().indices().get<by_pod_account>();
      auto itr = idx.lower_bound(boost::make_tuple(pod, ""));
      while (true) {
        if (itr == idx.end()) break;
        if (itr->pod != pod) break;
        if (itr->account != account) result.insert(itr->account);
        ++itr;
      }
    }
  }

  return result;
}

const myfeed_state_head_object *myfeed_plugin_impl::get_myfeed_state_head_object(const account_name_type &account) {
  try {
    return _db.find<myfeed_state_head_object, by_account>(account);
  }
  FC_CAPTURE_AND_RETHROW((account))
}

void myfeed_plugin_impl::insert(const account_name_type &account, const myfeed_object &item) {
  auto head = this->get_myfeed_state_head_object(account);
  if (head == nullptr) {
    head = &_db.create<myfeed_state_head_object>([&](myfeed_state_head_object &obj) { obj.account = account; });
  }

  // insert
  myfeed_key new_key{account, head->sequence};
//  ilog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item));
  boost::system::error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
//      ilog("duplicated key new_key=${new_key}, err=${err}", ("new_key", new_key)("err", ec.message()));
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to account head state
  _db.modify(*head, [&](myfeed_state_head_object& obj) { obj.sequence++; });

//  { // debug
//    if (account == "vit") {
//      ilog("vit-feed: item=${item}, new_key=${new_key}, head=${head}", ("item", item)("new_key", new_key)("head", *head));
//    }
//  }
}

void myfeed_plugin_impl::fanout(const account_name_type &account, const comment_object &comment) {
  std::set<account_name_type> audiences = find_audiences(account, comment.pod);
//  ilog("audiences: ${a}", ("a", audiences));
  myfeed_object feed_item(comment.author, chain::to_string(comment.permlink));
  for (const auto &item : audiences) {
    this->insert(item, feed_item);
  }
}

struct operation_visitor {
  operation_visitor(myfeed_plugin_impl &plugin) : _plugin(plugin) {}

  typedef void result_type;

  myfeed_plugin_impl &_plugin;

  template <typename T>
  void operator()(const T &) const {}

  /**
   * mostly to support old data, could be removed after HF2 applied
   * as creating new or editing using the same comment_operation, so we dont know if it is new comment or not here!
   */
  void operator()(const comment_operation &op) const {
    if (op.parent_author != WLS_ROOT_POST_PARENT) return;
//    ilog("found social_action_comment_create author=${a}, permlink=${p}", ("a", op.account)("p", action.permlink));
//    const comment_object &comment = _plugin._db.get_comment(op.author, op.permlink);
    const auto *comment = _plugin._db.find_comment(op.author, op.permlink);
    if (comment == nullptr) return;
    if (comment->last_update != comment->created) return; // ignore editing, put newly created post only.
//    if (_plugin._db.head_block_num() <= 28800) return; // period to import data from testnet2, ignore as it get duplicated feed when editing
//    ilog("author=${a}, permlink=${p}, last_update=${last_update}, created=${created}",
//        ("a", op.author)("p", op.permlink)("last_update", comment->last_update)("created", comment->created));

    _plugin.fanout(op.author, *comment);
  }

  void operator()(const social_action_operation &op) const {
    switch (op.action.which()) {
      case social_action::tag<social_action_comment_create>::value: {
        auto action = op.action.get<social_action_comment_create>();
        if (action.parent_author != WLS_ROOT_POST_PARENT) return;
//        ilog("found social_action_comment_create author=${a}, permlink=${p}", ("a", op.account)("p", action.permlink));
//        const comment_object &comment = _plugin._db.get_comment(op.account, action.permlink);

        const auto *comment = _plugin._db.find_comment(op.account, action.permlink);
        if (comment == nullptr) return;

        _plugin.fanout(op.account, *comment);
      } break;
    }
  }
};

void myfeed_plugin_impl::on_irreversible_block(uint32_t block_num) {
  /**
   * Work in this function:
   * - detect new root comment and share (vote)
   * - find author relationships (friends, pods, following)
   * - put new comment into their feed
   */

  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for myfeed_plugin");

  try {
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
      operation op = fc::raw::unpack<operation>(item.serialized_op);
      op.visit(operation_visitor(*this));
    }
  }
  FC_CAPTURE_AND_RETHROW()
}

}  // end namespace detail

myfeed_plugin::myfeed_plugin() {}

myfeed_plugin::~myfeed_plugin() {}

void myfeed_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
//  cli.add_options()(
//  );
  // clang-format on
  cfg.add(cli);
}

void myfeed_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  try {
    ilog("Initializing myfeed plugin");

    string data_dir = appbase::app().data_dir().string();
    if (data_dir == "") data_dir = ".";
    string path = data_dir + "/plugins/myfeed/";
    my = std::make_unique<detail::myfeed_plugin_impl>(*this, path);

    {
      bool replay = options.at("replay-blockchain").as<bool>();
      bool replay_blockchain = options.at("replay-blockchain").as<bool>();
      bool resync_blockchain = options.at("resync-blockchain").as<bool>();

      if (replay || replay_blockchain || resync_blockchain) my->_nudbstore_database.clean();
    }

    my->_nudbstore_database.open();
    my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });

    wls::chain::add_plugin_index<myfeed_state_head_object_index>(my->_db);

  } FC_CAPTURE_AND_RETHROW()
}

void myfeed_plugin::plugin_startup() {
  try {
    ilog("myfeed plugin: plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void myfeed_plugin::plugin_shutdown() {
  try {
    ilog("myfeed::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);

    my->_nudbstore_database.close();
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore<myfeed_key, myfeed_object>& myfeed_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

const myfeed_state_head_object *myfeed_plugin::get_myfeed_state_head_object(const account_name_type &account) {
  return my->get_myfeed_state_head_object(account);
}

}  // namespace myfeed
}  // namespace plugins
}  // namespace wls
