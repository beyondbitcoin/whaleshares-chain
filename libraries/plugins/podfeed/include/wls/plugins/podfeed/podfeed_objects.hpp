#pragma once

#include <wls/chain/wls_object_types.hpp>

#include <boost/multi_index/composite_key.hpp>

namespace wls {
namespace plugins {
namespace podfeed {

using namespace std;
using namespace wls::chain;
using namespace boost::multi_index;

using pod_name_type = account_name_type;

class podfeed_key {
 public:
  podfeed_key(const account_name_type &_account, const uint32_t _sequence)
      : account(_account), sequence(_sequence) {}

  podfeed_key() {}

  account_name_type account;
  uint32_t sequence = 0;
};

class podfeed_object {
 public:
  podfeed_object() {}

  podfeed_object(const account_name_type &_author, const std::string &permlink) : author(_author), permlink(permlink) {}

  account_name_type author;
  std::string permlink;

  std::set<std::string> shared_by;
};


//------------------------------------------------------------------------------

#ifndef PODFEED_SPACE_ID
#define PODFEED_SPACE_ID 16
#endif

enum podfeed_object_types {
  podfeed_state_object_type = (PODFEED_SPACE_ID << 8)
};

/**
 * to track last sequence of an account ( the head of the snake )
 */
class podfeed_state_head_object : public object<podfeed_state_object_type, podfeed_state_head_object> {
 public:
  template <typename Constructor, typename Allocator>
  podfeed_state_head_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  id_type id;

  pod_name_type pod;
  uint32_t sequence = 0;
};

//------------------------------------------------------------------------------

typedef podfeed_state_head_object::id_type podfeed_state_head_object_id_type;

// clang-format off
struct by_pod;
typedef multi_index_container<
    podfeed_state_head_object,
    indexed_by<
        ordered_unique< tag< by_id >, member< podfeed_state_head_object, podfeed_state_head_object_id_type, &podfeed_state_head_object::id > >,
        ordered_unique< tag< by_pod >, member< podfeed_state_head_object, pod_name_type, &podfeed_state_head_object::pod > >
    >,
    allocator< podfeed_state_head_object >
> podfeed_state_head_object_index;
// clang-format on


}  // namespace podfeed
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::podfeed::podfeed_key, (account)(sequence))
FC_REFLECT(wls::plugins::podfeed::podfeed_object, (author)(permlink)(shared_by))
FC_REFLECT(wls::plugins::podfeed::podfeed_state_head_object, (id)(pod)(sequence))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::podfeed::podfeed_state_head_object, wls::plugins::podfeed::podfeed_state_head_object_index)
