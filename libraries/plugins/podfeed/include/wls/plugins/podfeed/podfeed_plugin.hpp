#pragma once

#include <appbase/application.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/follow/follow_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>
#include <wls/plugins/podfeed/podfeed_objects.hpp>
#include <wls/nudbstore/nudbstore.hpp>

#define PODFEED_PLUGIN_NAME "podfeed"

namespace wls {
namespace plugins {
namespace podfeed {

namespace detail {
class podfeed_plugin_impl;
}

using namespace appbase;
using wls::protocol::account_name_type;
using nudbstore::nudbstore;

/**
 * Note: Considering
 * - using RocksDB for hot data (not payout)
 * - NuDB for archived data OR no need archived data at all.
 */
class podfeed_plugin : public plugin<podfeed_plugin> {
 public:
  podfeed_plugin();

  virtual ~podfeed_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin)(wls::plugins::follow::follow_plugin))

  static const std::string &name() {
    static std::string name = PODFEED_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  const podfeed_state_head_object* get_podfeed_state_head_object(const account_name_type &account);

  nudbstore<podfeed_key, podfeed_object>& get_nudbstore_database();

  // friend class detail::podfeed_plugin_impl;

 private:
  std::unique_ptr<detail::podfeed_plugin_impl> my;
};

}  // namespace podfeed
}  // namespace plugins
}  // namespace wls
