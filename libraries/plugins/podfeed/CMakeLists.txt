file(GLOB HEADERS "include/wls/plugins/podfeed/*.hpp")

add_library(podfeed_plugin
        podfeed_plugin.cpp
        )

target_link_libraries( podfeed_plugin block_history_plugin follow_plugin chain_plugin wls_chain wls_protocol wls_utilities )
target_include_directories(podfeed_plugin PUBLIC
        "${CMAKE_CURRENT_SOURCE_DIR}/include"
        "${CMAKE_CURRENT_SOURCE_DIR}/../../nudbstore/include"
        "${CMAKE_CURRENT_SOURCE_DIR}/../../vendor/NuDB/include"
        )

install(TARGETS
        podfeed_plugin

        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        )
