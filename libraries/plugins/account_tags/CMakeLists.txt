file(GLOB HEADERS "include/wls/plugins/account_tags/*.hpp")

add_library(account_tags_plugin
        account_tags_plugin.cpp)

target_link_libraries(account_tags_plugin block_history_plugin chain_plugin wls_chain wls_protocol wls_utilities)
target_include_directories(account_tags_plugin
        PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

install(TARGETS
        account_tags_plugin

        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        )
