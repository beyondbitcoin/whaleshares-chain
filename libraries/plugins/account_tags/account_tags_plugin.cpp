#include <wls/plugins/account_tags/account_tags_plugin.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
//#include <wls/app/impacted.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/protocol/config.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/hardfork.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/operation_notification.hpp>
#include <wls/chain/account_object.hpp>
#include <wls/chain/util/signal.hpp>

#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>
#include <fc/io/json.hpp>
#include <fc/string.hpp>

#include <boost/range/iterator_range.hpp>
#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace account_tags {

namespace detail {

using namespace wls::protocol;

class account_tags_plugin_impl {
 public:
  account_tags_plugin_impl(account_tags_plugin &_plugin) : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin) {
    _block_history_plugin = appbase::app().find_plugin<wls::plugins::block_history::block_history_plugin>();
  }

  virtual ~account_tags_plugin_impl();

  void on_irreversible_block(uint32_t block_num);

  void update_tags(const account_object &acc) const;

  account_metadata filter_tags(const account_object &acc) const;

  database &_db;
  account_tags_plugin &_self;

  boost::signals2::connection _irreversible_block_con;
  wls::plugins::block_history::block_history_plugin *_block_history_plugin;
};

account_tags_plugin_impl::~account_tags_plugin_impl() {}

account_metadata account_tags_plugin_impl::filter_tags(const account_object &acc) const {
  account_metadata meta;

  if (acc.json_metadata.size()) {
    try {
      meta = fc::json::from_string(to_string(acc.json_metadata)).as<account_metadata>();
    } catch (const fc::exception &e) {
      // Do nothing on malformed json_metadata
    }
  }

  // We need to write the transformed tags into a temporary local container because we can't modify meta.tags concurrently as we iterate over it.
  set<string> lower_tags;

  uint8_t tag_limit = 5;
  uint8_t count = 0;
  for (const string &tag : meta.profile.tags) {
    ++count;
    if (count > tag_limit || lower_tags.size() > tag_limit) break;
    if (tag == "") continue;
    lower_tags.insert(fc::to_lower(tag));
  }

  /// TODO: filter by fixed tags only
  lower_tags.insert(string());

  meta.profile.tags = lower_tags;

  return meta;
}

void account_tags_plugin_impl::update_tags(const account_object &acc) const {
  auto meta = filter_tags(acc);
  const auto &acctag_idx = _db.get_index<account_tag_index>().indices().get<by_account_tag>();
  auto citr = acctag_idx.lower_bound(acc.name);
  map<string, const account_tag_object *> existing_tags;
  vector<const account_tag_object *> remove_queue;

  while (citr != acctag_idx.end() && citr->account == acc.name) {
    const account_tag_object *tag = &*citr;
    ++citr;

    if (meta.profile.tags.find(tag->tag) == meta.profile.tags.end()) {
      remove_queue.push_back(tag);
    } else {
      existing_tags[tag->tag] = tag;
    }
  }

  for (const auto &tag : meta.profile.tags) {
    auto existing = existing_tags.find(tag);

    if (existing == existing_tags.end()) {
      _db.create<account_tag_object>([&](account_tag_object &obj) {
        obj.tag = tag;
        obj.account = acc.name;
      });
    }
  }

  for (const auto &item : remove_queue) {
    _db.remove(*item);
  }
}

void account_tags_plugin_impl::on_irreversible_block(uint32_t block_num) {
  FC_ASSERT(_block_history_plugin != nullptr, "block_history_plugin is required for account_tags_plugin");

  try { /// plugins shouldn't ever throw
    vector<wls::plugins::block_history::block_history_operation_object> ops = _block_history_plugin->get_ops_recent_block(block_num);
    for (const auto &item : ops) {
      operation op = fc::raw::unpack<operation>(item.serialized_op);
      switch (op.which()) {
        case operation::tag<account_create_operation>::value: {
          auto aco = op.get<account_create_operation>();
          const auto& acc = _db.get_account(aco.new_account_name);
          update_tags(acc);
        } break;
        case operation::tag<account_update_operation>::value: {
          auto aao = op.get<account_update_operation>();
          const auto& acc = _db.get_account(aao.account);
          update_tags(acc);
        } break;
        default:
          break;
      }
    }
  } catch (const fc::exception &e) {
    edump((e.to_detail_string()));
  } catch (...) {
    elog("unhandled exception");
  }
}

}  // namespace detail

account_tags_plugin::account_tags_plugin() {}

account_tags_plugin::~account_tags_plugin() {}

void account_tags_plugin::set_program_options(boost::program_options::options_description &cli, boost::program_options::options_description &cfg) {}

void account_tags_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  ilog("Initializing account_tags plugin");
  my = std::make_unique<detail::account_tags_plugin_impl>(*this);

  add_plugin_index<account_tag_index>(my->_db);

  my->_irreversible_block_con = my->_db.on_irreversible_block.connect([&](uint32_t block_num) { my->on_irreversible_block(block_num); });
}

void account_tags_plugin::plugin_startup() {
  try {
    ilog("account_tags_plugin::plugin_startup()");
  } FC_CAPTURE_AND_RETHROW()
}

void account_tags_plugin::plugin_shutdown() {
  try {
    ilog("account_tags_plugin::plugin_shutdown");
    chain::util::disconnect_signal(my->_irreversible_block_con);
  } FC_CAPTURE_AND_RETHROW()
}

}  // namespace account_tags
}  // namespace plugins
}  // namespace wls
