#include <wls/plugins/network_broadcast_api/network_broadcast_api_plugin.hpp>
#include <wls/plugins/network_broadcast_api/network_broadcast_api.hpp>

namespace wls {
namespace plugins {
namespace network_broadcast_api {

network_broadcast_api_plugin::network_broadcast_api_plugin() {}

network_broadcast_api_plugin::~network_broadcast_api_plugin() {}

void network_broadcast_api_plugin::set_program_options(options_description &cli, options_description &cfg) {}

void network_broadcast_api_plugin::plugin_initialize(const variables_map &options) {
  ilog("Initializing network_broadcast_api plugin");
  api = std::make_shared<network_broadcast_api>();
}

void network_broadcast_api_plugin::plugin_startup() {}

void network_broadcast_api_plugin::plugin_shutdown() { ilog("network_broadcast_api_plugin::plugin_shutdown"); }

}  // namespace network_broadcast_api
}  // namespace plugins
}  // namespace wls
