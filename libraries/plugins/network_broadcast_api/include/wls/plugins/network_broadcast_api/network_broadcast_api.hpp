#pragma once

#include <wls/plugins/json_rpc/utility.hpp>
#include <wls/protocol/types.hpp>
#include <wls/protocol/transaction.hpp>
#include <wls/plugins/json_rpc/utility.hpp>
#include <wls/protocol/block.hpp>

#include <fc/optional.hpp>
#include <fc/variant.hpp>
#include <fc/vector.hpp>

namespace wls {
namespace plugins {
namespace network_broadcast_api {

using wls::plugins::json_rpc::void_type;
using wls::protocol::signed_block;
using wls::protocol::signed_transaction;
using wls::protocol::transaction_id_type;

struct broadcast_transaction_args {
  signed_transaction trx;
  int32_t max_block_age = -1;
};

typedef void_type broadcast_transaction_return;

struct broadcast_block_args {
  signed_block block;
};

typedef fc::variant broadcast_block_return;

typedef vector<variant> broadcast_transaction_synchronous_args;

struct broadcast_transaction_synchronous_return {
  broadcast_transaction_synchronous_return() {}

  broadcast_transaction_synchronous_return(transaction_id_type txid, int32_t bn, int32_t tn, bool ex) : id(txid), block_num(bn), trx_num(tn), expired(ex) {}

  transaction_id_type id;
  int32_t block_num = 0;
  int32_t trx_num = 0;
  bool expired = false;
};

typedef std::function<void(const broadcast_transaction_synchronous_return &)> confirmation_callback;

namespace detail {
class network_broadcast_api_impl;
}

class network_broadcast_api {
 public:
  network_broadcast_api();

  ~network_broadcast_api();

  DECLARE_API((broadcast_transaction)(broadcast_transaction_synchronous)(broadcast_block)
              //               (broadcast_transaction_with_callback)
  )

 private:
  std::unique_ptr<detail::network_broadcast_api_impl> my;
};

}  // namespace network_broadcast_api
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::network_broadcast_api::broadcast_transaction_args, (trx)(max_block_age))
FC_REFLECT(wls::plugins::network_broadcast_api::broadcast_block_args, (block))
FC_REFLECT(wls::plugins::network_broadcast_api::broadcast_transaction_synchronous_return, (id)(block_num)(trx_num)(expired))
