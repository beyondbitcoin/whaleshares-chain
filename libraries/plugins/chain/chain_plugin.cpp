#include <wls/chain/database_exceptions.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
//#include <wls/protocol/config.hpp>

#include <fc/string.hpp>
#include <fc/network/ip.hpp>
#include <fc/network/resolve.hpp>
#include <fc/thread/thread.hpp>
#include <fc/io/json.hpp>

#include <boost/asio.hpp>
#include <boost/optional.hpp>
#include <boost/bind.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/thread/future.hpp>
#include <boost/lockfree/queue.hpp>

#include <thread>
#include <memory>
#include <iostream>

namespace wls {
namespace plugins {
namespace chain {

using namespace wls;
using fc::flat_map;
using wls::chain::block_id_type;
namespace asio = boost::asio;

struct generate_block_request {
  generate_block_request(const fc::time_point_sec w, const account_name_type &wo, const fc::ecc::private_key &priv_key, uint32_t s)
      : when(w), witness_owner(wo), block_signing_private_key(priv_key), skip(s) {}

  const fc::time_point_sec when;
  const account_name_type &witness_owner;
  const fc::ecc::private_key &block_signing_private_key;
  uint32_t skip;
  signed_block block;
};

typedef fc::static_variant<const signed_block *, const signed_transaction *, generate_block_request *> write_request_ptr;
typedef fc::static_variant<boost::promise<void> *, fc::future<void> *> promise_ptr;

struct write_context {
  write_request_ptr req_ptr;
  uint32_t skip = 0;
  bool success = true;
  fc::optional<fc::exception> except;
  promise_ptr prom_ptr;
};

namespace detail {

class chain_plugin_impl {
 public:
  chain_plugin_impl() {}

  ~chain_plugin_impl() {}

  uint64_t shared_memory_size = 0;
  bfs::path shared_memory_dir;
  bool replay = false;
  bool resync = false;
  bool readonly = false;
  bool check_locks = false;
  bool validate_invariants = false;
  //               uint32_t stop_replay_at = 0;
  uint32_t flush_interval = 0;
  flat_map<uint32_t, block_id_type> loaded_checkpoints;
  uint32_t max_undo = WLS_MAX_UNDO_HISTORY;

  uint32_t allow_future_time = 5;

  bool running = true;

  database db;
};

struct write_request_visitor {
  write_request_visitor() {}

  database *db;
  uint32_t skip = 0;
  fc::optional<fc::exception> *except;

  typedef bool result_type;

  bool operator()(const signed_block *block) {
    bool result = false;

    try {
      result = db->push_block(*block, skip);
    } catch (fc::exception &e) {
      *except = e;
    } catch (...) {
      *except = fc::unhandled_exception(FC_LOG_MESSAGE(warn, "Unexpected exception while pushing block."), std::current_exception());
    }

    return result;
  }

  bool operator()(const signed_transaction *trx) {
    bool result = false;

    try {
      db->push_transaction(*trx);
      result = true;
    } catch (fc::exception &e) {
      *except = e;
    } catch (...) {
      *except = fc::unhandled_exception(FC_LOG_MESSAGE(warn, "Unexpected exception while pushing block."), std::current_exception());
    }

    return result;
  }

  bool operator()(generate_block_request *req) {
    bool result = false;

    try {
      req->block = db->generate_block(req->when, req->witness_owner, req->block_signing_private_key, req->skip);

      result = true;
    } catch (fc::exception &e) {
      *except = e;
    } catch (...) {
      *except = fc::unhandled_exception(FC_LOG_MESSAGE(warn, "Unexpected exception while pushing block."), std::current_exception());
    }

    return result;
  }
};

struct request_promise_visitor {
  request_promise_visitor() {}

  typedef void result_type;

  template <typename T>
  void operator()(T *t) {
    t->set_value();
  }
};

}  // namespace detail

chain_plugin::chain_plugin() : my(new detail::chain_plugin_impl()) {}

chain_plugin::~chain_plugin() {}

database &chain_plugin::db() { return my->db; }

const wls::chain::database &chain_plugin::db() const { return my->db; }

bfs::path chain_plugin::state_storage_dir() const { return my->shared_memory_dir; }

void chain_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
  cfg.add_options()
     ("shared-file-dir", bpo::value<bfs::path>()->default_value("blockchain"),
      "the location of the chain shared memory files (absolute path or relative to application data dir)")
     ("shared-file-size", bpo::value<string>()->default_value("2G"),
      "Size of the shared memory file. Default: 2G. If running a full node, increase this value to 16G.")
     ("checkpoint,c", bpo::value<vector<string>>()->composing(), "Pairs of [BLOCK_NUM,BLOCK_ID] that should be enforced as checkpoints.")
     ("flush-state-interval", bpo::value<uint32_t>(), "flush shared memory changes to disk every N blocks")
     ("max-undo", bpo::value< uint32_t >()->default_value(WLS_MAX_UNDO_HISTORY), "WLS_MAX_UNDO_HISTORY, default = 10000.");
  cli.add_options()
     ("replay-blockchain", bpo::bool_switch()->default_value(false), "clear chain database and replay all blocks")
     ("resync-blockchain", bpo::bool_switch()->default_value(false), "clear chain database and block log")
     ("stop-replay-at-block", bpo::value<uint32_t>(), "Stop and exit after reaching given block number")
     ("advanced-benchmark", "Make profiling for every plugin.")
     ("set-benchmark-interval", bpo::value<uint32_t>(), "Print time and memory usage every given number of blocks")
     ("dump-memory-details", bpo::bool_switch()->default_value(false),
      "Dump database objects memory usage info. Use set-benchmark-interval to set dump interval.")
     ("check-locks", bpo::bool_switch()->default_value(false), "Check correctness of chainbase locking")
     ("validate-database-invariants", bpo::bool_switch()->default_value(false), "Validate all supply invariants check out")
#ifdef IS_TEST_NET
     ("chain-id", bpo::value<std::string>()->default_value(WLS_CHAIN_ID), "chain ID to connect to")
#endif
               ;
  // clang-format on
}

void chain_plugin::plugin_initialize(const variables_map &options) {
  ilog("Initializing chain plugin");

  // add _max_undo option
  if (options.count("max-undo")) {
    my->max_undo = options.at("max-undo").as<uint32_t>();
  }
  elog("max-undo is ${n}", ("n", my->max_undo));

  my->shared_memory_dir = app().data_dir() / "blockchain";
  if (options.count("shared-file-dir")) {
    auto sfd = options.at("shared-file-dir").as<bfs::path>();
    if (sfd.is_relative())
      my->shared_memory_dir = app().data_dir() / sfd;
    else
      my->shared_memory_dir = sfd;
  }
  my->shared_memory_size = fc::parse_size(options.at("shared-file-size").as<string>());
  my->replay = options.at("replay-blockchain").as<bool>();
  my->resync = options.at("resync-blockchain").as<bool>();
  my->check_locks = options.at("check-locks").as<bool>();
  my->validate_invariants = options.at("validate-database-invariants").as<bool>();
  if (options.count("flush-state-interval"))
    my->flush_interval = options.at("flush-state-interval").as<uint32_t>();
  else
    my->flush_interval = 10000;

  if (options.count("checkpoint")) {
    auto cps = options.at("checkpoint").as<vector<string>>();
    my->loaded_checkpoints.reserve(cps.size());
    for (const auto &cp : cps) {
      auto item = fc::json::from_string(cp).as<std::pair<uint32_t, block_id_type>>();
      my->loaded_checkpoints[item.first] = item.second;
    }
  }
}

void chain_plugin::plugin_startup() {
  ilog("Starting chain with shared_file_size: ${n} bytes", ("n", my->shared_memory_size));

  if (my->resync) {
    wlog("resync requested: deleting block log and shared memory");
    my->db.wipe(app().data_dir() / "blockchain", my->shared_memory_dir, true);
  }

  my->db.set_flush_interval(my->flush_interval);
  my->db.add_checkpoints(my->loaded_checkpoints);
  my->db.set_require_locking(my->check_locks);
  my->db.set_max_undo(my->max_undo);

  if (my->replay) {
    ilog("Replaying blockchain on user request.");
    my->db.reindex(app().data_dir() / "blockchain", my->shared_memory_dir, my->shared_memory_size);
  } else {
    try {
      ilog("Opening shared memory from ${path}", ("path", my->shared_memory_dir.generic_string()));
      my->db.open(app().data_dir() / "blockchain", my->shared_memory_dir, WLS_INIT_SUPPLY, my->shared_memory_size, chainbase::database::read_write);
    } catch (const fc::exception &e) {
      wlog("Error opening database, attempting to replay blockchain. Error: ${e}", ("e", e));

      try {
        my->db.reindex(app().data_dir() / "blockchain", my->shared_memory_dir, my->shared_memory_size);
      } catch (wls::chain::block_log_exception &) {
        wlog("Error opening block log. Having to resync from network...");
        my->db.open(app().data_dir() / "blockchain", my->shared_memory_dir, WLS_INIT_SUPPLY, my->shared_memory_size, chainbase::database::read_write);
      }
    }
  }

  ilog("Started on blockchain with ${n} blocks", ("n", my->db.head_block_num()));
  on_sync();
}

void chain_plugin::plugin_shutdown() {
  ilog("closing chain database");
  //            my->stop_write_processing();
  my->db.close();
  ilog("database closed successfully");
}

bool chain_plugin::accept_block(const wls::chain::signed_block &block, bool currently_syncing, uint32_t skip) {
  if (currently_syncing && block.block_num() % 10000 == 0) {
    ilog("Syncing Blockchain --- Got block: #${n} time: ${t} producer: ${p}", ("t", block.timestamp)("n", block.block_num())("p", block.witness));
  }

  check_time_in_block(block);

  return my->db.push_block(block, skip);

//  std::promise<bool> promise;
//  auto result = promise.get_future();
//
//  appbase::app().get_io_service().post([&] {
//    try {
//      promise.set_value(my->db.push_block(block, skip));
//    } catch (...) {
//      promise.set_exception(std::current_exception());
//    }
//  });
//  return result.get();  // if an exception was, it will be thrown
}

void chain_plugin::accept_transaction(const wls::chain::signed_transaction &trx) {
  my->db.push_transaction(trx);

//  std::promise<bool> promise;
//  auto wait = promise.get_future();
//
//  appbase::app().get_io_service().post([&] {
//    try {
//      my->db.push_transaction(trx);
//      promise.set_value(true);
//    } catch (...) {
//      promise.set_exception(std::current_exception());
//    }
//  });
//  wait.get();  // if an exception was, it will be thrown
}

wls::chain::signed_block chain_plugin::generate_block(const fc::time_point_sec when, const account_name_type &witness_owner,
                                                      const fc::ecc::private_key &block_signing_private_key, uint32_t skip) {
  wls::chain::signed_block block = my->db.generate_block(when, witness_owner, block_signing_private_key, skip);

  return block;
}

bool chain_plugin::block_is_on_preferred_chain(const wls::chain::block_id_type &block_id) {
  // If it's not known, it's not preferred.
  if (!db().is_known_block(block_id)) return false;

  // Extract the block number from block_id, and fetch that block number's ID from the database.
  // If the database's block ID matches block_id, then block_id is on the preferred chain. Otherwise, it's on a fork.
  return db().get_block_id_for_num(wls::chain::block_header::num_from_id(block_id)) == block_id;
}

void chain_plugin::check_time_in_block(const wls::chain::signed_block &block) {
  time_point_sec now = fc::time_point::now();

  uint64_t max_accept_time = now.sec_since_epoch();
  max_accept_time += my->allow_future_time;
  FC_ASSERT(block.timestamp.sec_since_epoch() <= max_accept_time);
}

}  // namespace chain
}  // namespace plugins
}  // namespace wls
