#pragma once

#include <appbase/application.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/comment_object.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/block_history/block_history_plugin.hpp>

#include <fc/thread/future.hpp>

#include <boost/multi_index/composite_key.hpp>

#define PODTAGS_PLUGIN_NAME "podtags"

namespace wls {
namespace plugins {
namespace podtags {

using namespace appbase;
using namespace wls::chain;
using namespace boost::multi_index;
using chainbase::allocator;
using chainbase::object;
using chainbase::oid;

using pod_name_type = account_name_type;

#ifndef PODTAG_SPACE_ID
#define PODTAG_SPACE_ID 23
#endif

typedef fc::fixed_string_32 tag_name_type;

enum {
  podtag_object_type = (PODTAG_SPACE_ID << 8)
};

namespace detail {
class podtags_plugin_impl;
}

/**
 *  The purpose of the tag object is to allow to index and filter all pods by a string tag.
 *  When ever a pod is modified, all tag_objects for that pod are updated to match.
 */
class podtag_object : public object<podtag_object_type, podtag_object> {
 public:
  template <typename Constructor, typename Allocator>
  podtag_object(Constructor &&c, allocator<Allocator> a) {
    c(*this);
  }

  podtag_object() {}

  id_type id;

  tag_name_type tag;
  pod_name_type pod;
};

typedef oid<podtag_object> podtag_id_type;

// clang-format off
struct by_tag_pod;
struct by_pod_tag;

typedef multi_index_container<
  podtag_object,
  indexed_by<
     ordered_unique<tag<by_id>, member<podtag_object, podtag_id_type, &podtag_object::id> >,
     ordered_unique< tag< by_tag_pod >,
       composite_key< podtag_object,
         member< podtag_object, tag_name_type, &podtag_object::tag >,
         member< podtag_object, pod_name_type, &podtag_object::pod >
       >,
      composite_key_compare < std::less<tag_name_type>, std::less<pod_name_type> >
     >,
     ordered_unique< tag< by_pod_tag >,
      composite_key< podtag_object,
        member< podtag_object, pod_name_type, &podtag_object::pod >,
        member< podtag_object, tag_name_type, &podtag_object::tag >
      >,
      composite_key_compare < std::less<pod_name_type>, std::less<tag_name_type> >
    >
  >,
  allocator<podtag_object>
> podtag_index;
// clang-format on

/**
 * Used to parse the metadata from the pod json_meta field.
 */
struct pod_metadata {
  set<string> tags;
};

class podtags_plugin : public appbase::plugin<podtags_plugin> {
 public:
  podtags_plugin();

  virtual ~podtags_plugin();

  APPBASE_PLUGIN_REQUIRES((wls::plugins::chain::chain_plugin)(wls::plugins::block_history::block_history_plugin))

  static const std::string &name() {
    static std::string name = PODTAGS_PLUGIN_NAME;
    return name;
  }

  virtual void set_program_options(options_description &cli, options_description &cfg) override;

  virtual void plugin_initialize(const boost::program_options::variables_map &options) override;

  virtual void plugin_startup() override;

  virtual void plugin_shutdown() override;

  friend class detail::podtags_plugin_impl;

  std::unique_ptr<detail::podtags_plugin_impl> my;
};

}  // namespace podtags
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::podtags::pod_metadata, (tags));
FC_REFLECT(wls::plugins::podtags::podtag_object, (id)(tag)(pod))
CHAINBASE_SET_INDEX_TYPE(wls::plugins::podtags::podtag_object, wls::plugins::podtags::podtag_index)

