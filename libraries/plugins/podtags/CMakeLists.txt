file(GLOB HEADERS "include/wls/plugins/podtags/*.hpp")

add_library(podtags_plugin
        podtags_plugin.cpp)

target_link_libraries(podtags_plugin block_history_plugin chain_plugin wls_chain wls_protocol wls_utilities)
target_include_directories(podtags_plugin
        PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

install(TARGETS
        podtags_plugin

        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        )
