#pragma once

#include <wls/plugins/database_api/database_api_args.hpp>
#include <wls/plugins/json_rpc/utility.hpp>

#include <wls/protocol/types.hpp>

#include <fc/optional.hpp>
#include <fc/variant.hpp>
#include <fc/vector.hpp>

namespace wls {
namespace plugins {
namespace database_api {

namespace detail {
class database_api_impl;
}

class database_api {
 public:
  database_api();

  ~database_api();
  // clang-format off
  DECLARE_API(
     (get_health_check)
     (get_trending_tags)
     (get_state)
     (get_active_witnesses)
     (get_block_header)
     (get_block)
     (get_blocks_missed)
     (get_ops_in_block)
     (get_ops_in_blocks)
     (get_config)
//   (get_schema)
     (get_dynamic_global_properties)
     (get_chain_properties)
     (get_witness_schedule)
     (get_hardfork_version)
     (get_next_scheduled_hardfork)
     (get_reward_fund)
     (get_vesting_payout_fund)
     (get_key_references)
     (get_accounts)
     (get_accounts_by_tag)
     (lookup_account_names)
     (lookup_accounts)
     (get_account_count)
     (get_withdraw_routes)
     (get_account_bandwidth)
     (get_account_daily_bandwidth)
     (get_witnesses)
     (get_witness_by_account)
     (get_witnesses_by_vote)
     (lookup_witness_accounts)
     (get_witness_count)
     (get_transaction_hex)
     (get_required_signatures)
     (get_potential_signatures)
     (verify_authority)
     (verify_account_authority)
     (get_active_votes)
     (get_account_votes)
     (get_content)
     (get_content_replies)
     (get_tags_used_by_author)
     (get_discussions_by_payout)
     (get_post_discussions_by_payout)
     (get_comment_discussions_by_payout)
     (get_discussions_by_trending)
     (get_discussions_by_created)
     (get_discussions_by_active)
     (get_discussions_by_cashout)
     (get_discussions_by_votes)
     (get_discussions_by_children)
     (get_discussions_by_hot)
     (get_discussions_by_feed)
     (get_discussions_by_blog)
     (get_discussions_by_comments)
     (get_replies_by_last_update)
     (get_discussions_by_author_before_date)
     (get_account_history)
     (get_account_notification)
     (lookup_pods)
     (get_pods)
     (get_pods_by_tag)
     (lookup_pod_members)
     (get_pod_members_by_user)
     (get_pod_join_requests)
     (get_pod_join_requests_by_user)
     (get_friends)
     (get_friend_requests_from)
     (get_friend_requests_to)
     (get_user_feed)
     (get_pod_feed)
     (get_pod_announcements)
     (get_blog_feed)
     (get_shares_feed)
     (get_htlcs_from)
     (get_htlcs_to)
     (get_tips_from)
     (get_tips_to)
  )
  // clang-format on

  uint32_t max_blocks = 10; // limit n blocks in get_ops_in_blocks, set to 0 to disable get_ops_in_blocks method

 private:
  std::unique_ptr<detail::database_api_impl> my;
};

}  // namespace database_api
}  // namespace plugins
}  // namespace wls
