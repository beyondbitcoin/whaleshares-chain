#pragma once

#include <wls/plugins/tags/tags_plugin.hpp>
#include <wls/plugins/witness/witness_objects.hpp>
#include <wls/plugins/block_history/block_history_objects.hpp>
#include <wls/plugins/block_missed/block_missed_objects.hpp>

#include <wls/chain/wls_object_types.hpp>
#include <wls/chain/account_object.hpp>
#include <wls/chain/pod_objects.hpp>

namespace wls {
namespace plugins {
namespace database_api {

using namespace wls::chain;
using namespace wls::protocol;
using namespace std;

struct api_context;

struct health_check {
  uint16_t status = 200; /** http status, 200: OK, 503: error */
  string message = "HEALTH_CHECK_OK";
};

struct scheduled_hardfork {
  hardfork_version hf_version;
  fc::time_point_sec live_time;
};

struct liquidity_balance {
  string account;
  fc::uint128_t weight;
};

struct withdraw_route {
  string from_account;
  string to_account;
  uint16_t percent;
  bool auto_vest;
};

enum withdraw_route_type { incoming, outgoing, all };

/**
 *  Defines the arguments to a query as a struct so it can be easily extended
 */
struct discussion_query {
  void validate() const {
    FC_ASSERT(filter_tags.find(tag) == filter_tags.end());
    FC_ASSERT(limit <= 100);
  }

  string tag;
  uint32_t limit = 0;
  set<string> filter_tags;
  set<string> select_authors;  ///< list of authors to include, posts not by this author are filtered
  set<string> select_tags;     ///< list of tags to include, posts without these tags are filtered
  uint32_t truncate_body = 0;  ///< the number of bytes of the post body to return, 0 for all
  optional<string> start_author;
  optional<string> start_permlink;
  optional<string> parent_author;
  optional<string> parent_permlink;
};

/////////////////////////////////////////////////////////////////////////////////////

typedef chain::block_summary_object block_summary_api_obj;
typedef chain::comment_vote_object comment_vote_api_obj;
typedef chain::withdraw_vesting_route_object withdraw_vesting_route_api_obj;
typedef chain::witness_vote_object witness_vote_api_obj;
typedef chain::witness_schedule_object witness_schedule_api_obj;
typedef chain::reward_fund_object reward_fund_api_obj;
typedef chain::escrow_object vesting_reward_api_obj;
typedef witness::account_bandwidth_object account_bandwidth_api_obj;
typedef witness::account_daily_bandwidth_object account_daily_bandwidth_api_obj;
//typedef chain::pod_object pod_api_obj;
typedef chain::pod_member_object pod_member_api_obj;
typedef chain::pod_join_request_object pod_join_request_api_obj;
typedef chain::friend_request_object friend_request_api_obj;
typedef chain::htlc_object htlc_api_obj;
typedef wls::plugins::block_missed::block_missed_object block_missed_api_obj;

struct pod_api_obj {
  pod_api_obj(const chain::pod_object &p, const chain::database &db) :
  id(p.id),
  name(p.name),
  created(p.created),
  fee(p.fee),
  json_metadata(to_string(p.json_metadata)),
  allow_join(p.allow_join),
  member_count(p.member_count)
  {
    const auto creator = db.find_account(name);
    if (creator != nullptr) {
      json_metadata_creator = to_string(creator->json_metadata);
    }
  }

  pod_api_obj() {}

  pod_object_id_type id;

  pod_name_type name;
  time_point_sec created;
  asset fee = asset(0, WLS_SYMBOL);
  string json_metadata;
  bool allow_join = true;
  uint16_t member_count = 0;

  string json_metadata_creator;
};

// clang-format off
struct comment_api_obj {
  comment_api_obj(const chain::comment_object &o) :
     id(o.id),
     category(to_string(o.category)),
     parent_author(o.parent_author),
     parent_permlink(to_string(o.parent_permlink)),
     author(o.author),
     permlink(to_string(o.permlink)),
     title(to_string(o.title)),
     body(to_string(o.body)),
     json_metadata(to_string(o.json_metadata)),
     last_update(o.last_update),
     created(o.created),
     active(o.active),
     last_payout(o.last_payout),
     depth(o.depth),
     children(o.children),
     net_rshares(o.net_rshares),
     abs_rshares(o.abs_rshares),
     vote_rshares(o.vote_rshares),
     children_abs_rshares(o.children_abs_rshares),
     cashout_time(o.cashout_time),
     max_cashout_time(o.max_cashout_time),
     total_vote_weight(o.total_vote_weight),
     reward_weight(o.reward_weight),
     total_payout_value(o.total_payout_value),
     curator_payout_value(o.curator_payout_value),
     author_rewards(o.author_rewards),
     net_votes(o.net_votes),
     root_comment(o.root_comment),
     max_accepted_payout(o.max_accepted_payout),
     allow_replies(o.allow_replies),
     allow_votes(o.allow_votes),
     allow_curation_rewards(o.allow_curation_rewards),
     pod(o.pod),
     allow_friends(o.allow_friends) {

     for (auto &route : o.beneficiaries) {
        beneficiaries.push_back(route);
     }
  }

  comment_api_obj() {}

  comment_id_type id;
  string category;
  account_name_type parent_author;
  string parent_permlink;
  account_name_type author;
  string permlink;

  string title;
  string body;
  string json_metadata;
  time_point_sec last_update;
  time_point_sec created;
  time_point_sec active;
  time_point_sec last_payout;

  uint8_t depth = 0;
  uint32_t children = 0;

  share_type net_rshares;
  share_type abs_rshares;
  share_type vote_rshares;

  share_type children_abs_rshares;
  time_point_sec cashout_time;
  time_point_sec max_cashout_time;
  uint64_t total_vote_weight = 0;

  uint16_t reward_weight = 0;

  asset total_payout_value;
  asset curator_payout_value;

  share_type author_rewards;

  int32_t net_votes = 0;

  comment_id_type root_comment;

  asset max_accepted_payout;
  bool allow_replies = false;
  bool allow_votes = false;
  bool allow_curation_rewards = false;
  vector<beneficiary_route_type> beneficiaries;

  pod_name_type pod;
  bool allow_friends = false;
};

struct tag_api_obj {
  tag_api_obj(const tags::tag_stats_object &o) :
     name(o.tag),
     total_payouts(o.total_payout),
     net_votes(o.net_votes),
     top_posts(o.top_posts),
     comments(o.comments),
     trending(o.total_trending) {}

  tag_api_obj() {}

  string name;
  asset total_payouts;
  int32_t net_votes = 0;
  uint32_t top_posts = 0;
  uint32_t comments = 0;
  fc::uint128 trending = 0;
};

struct account_api_obj {
  account_api_obj(const chain::account_object &a, const chain::database &db) :
     id(a.id),
     name(a.name),
     memo_key(a.memo_key),
     json_metadata(to_string(a.json_metadata)),
     proxy(a.proxy),
     last_account_update(a.last_account_update),
     created(a.created),
     recovery_account(a.recovery_account),
     comment_count(a.comment_count),
     lifetime_vote_count(a.lifetime_vote_count),
     post_count(a.post_count),
     voting_power(a.voting_power),
     last_vote_time(a.last_vote_time),
     balance(a.balance),
     reward_steem_balance(a.reward_steem_balance),
     reward_vesting_balance(a.reward_vesting_balance),
     reward_vesting_steem(a.reward_vesting_steem),
     curation_rewards(a.curation_rewards),
     posting_rewards(a.posting_rewards),
     vesting_shares(a.vesting_shares),
     vesting_withdraw_rate(a.vesting_withdraw_rate),
     next_vesting_withdrawal(a.next_vesting_withdrawal),
     withdrawn(a.withdrawn),
     to_withdraw(a.to_withdraw),
     withdraw_routes(a.withdraw_routes),
     witnesses_voted_for(a.witnesses_voted_for),
     last_post(a.last_post),
     last_root_post(a.last_root_post),
     vr_snaphost_next_cycle(a.vr_snaphost_next_cycle),
     vr_snaphost_shares(a.vr_snaphost_shares),
     vr_snaphost_cycle(a.vr_snaphost_cycle),
     vr_claimed_cycle(a.vr_claimed_cycle),
     vr_claimed_amount(a.vr_claimed_amount)
     {

     size_t n = a.proxied_vsf_votes.size();
     proxied_vsf_votes.reserve(n);
     for (size_t i = 0; i < n; i++)
        proxied_vsf_votes.push_back(a.proxied_vsf_votes[i]);

     const auto &auth = db.get<account_authority_object, by_account>(name);
     owner = authority(auth.owner);
     active = authority(auth.active);
     posting = authority(auth.posting);
     last_owner_update = auth.last_owner_update;

     if (db.has_index<witness::account_bandwidth_index>()) {
        auto forum_bandwidth = db.find<witness::account_bandwidth_object, witness::by_account_bandwidth_type>(
           boost::make_tuple(name, witness::bandwidth_type::forum));

        if (forum_bandwidth != nullptr) {
           average_bandwidth = forum_bandwidth->average_bandwidth;
           lifetime_bandwidth = forum_bandwidth->lifetime_bandwidth;
           last_bandwidth_update = forum_bandwidth->last_bandwidth_update;
        }
     }

    if (db.has_index<witness::account_daily_bandwidth_index>()) {
      auto bw = db.find<witness::account_daily_bandwidth_object, witness::by_account>(name);
      if (bw != nullptr) {
        daily_bandwidth = bw->bandwidth;
        last_daily_bandwidth_update = bw->last_update;
      }
    }

    const auto &pod_obj = db.find_pod(name);
    if (pod_obj != nullptr) {
      is_pod = true;
    }
  }


  account_api_obj() {}

  account_id_type id;

  account_name_type name;
  authority owner;
  authority active;
  authority posting;
  public_key_type memo_key;
  string json_metadata;
  account_name_type proxy;

  time_point_sec last_owner_update;
  time_point_sec last_account_update;

  time_point_sec created;
  account_name_type recovery_account;
  uint32_t comment_count = 0;
  uint32_t lifetime_vote_count = 0;
  uint32_t post_count = 0;

  uint16_t voting_power = 0;
  time_point_sec last_vote_time;

  asset balance;

  asset reward_steem_balance;
  asset reward_vesting_balance;
  asset reward_vesting_steem;

  share_type curation_rewards;
  share_type posting_rewards;

  asset vesting_shares;
  asset vesting_withdraw_rate;
  time_point_sec next_vesting_withdrawal;
  share_type withdrawn;
  share_type to_withdraw;
  uint16_t withdraw_routes = 0;

  vector<share_type> proxied_vsf_votes;

  uint16_t witnesses_voted_for;

  share_type average_bandwidth = 0;
  share_type lifetime_bandwidth = 0;
  time_point_sec last_bandwidth_update;

  time_point_sec last_post;
  time_point_sec last_root_post;

  uint32_t daily_bandwidth = 0;
  time_point_sec last_daily_bandwidth_update;

  uint32_t  vr_snaphost_next_cycle  = 0;
  asset     vr_snaphost_shares      = asset(0, VESTS_SYMBOL);
  uint32_t  vr_snaphost_cycle       = 0;
  uint32_t  vr_claimed_cycle        = 0;
  asset     vr_claimed_amount       = asset(0, WLS_SYMBOL);

  bool is_pod = false;
};

struct account_history_api_obj {

};

struct witness_api_obj {
  witness_api_obj(const chain::witness_object &w) :
     id(w.id),
     owner(w.owner),
     created(w.created),
     url(to_string(w.url)),
     total_missed(w.total_missed),
     last_aslot(w.last_aslot),
     last_confirmed_block_num(w.last_confirmed_block_num),
     signing_key(w.signing_key),
     props(w.props),
     votes(w.votes),
     virtual_last_update(w.virtual_last_update),
     virtual_position(w.virtual_position),
     virtual_scheduled_time(w.virtual_scheduled_time),
     last_work(w.last_work),
     running_version(w.running_version),
     hardfork_version_vote(w.hardfork_version_vote),
     hardfork_time_vote(w.hardfork_time_vote) {}

  witness_api_obj() {}

  witness_id_type id;
  account_name_type owner;
  time_point_sec created;
  string url;
  uint32_t total_missed = 0;
  uint64_t last_aslot = 0;
  uint64_t last_confirmed_block_num = 0;
  public_key_type signing_key;
  chain_properties props;
  share_type votes;
  fc::uint128 virtual_last_update;
  fc::uint128 virtual_position;
  fc::uint128 virtual_scheduled_time;
  digest_type last_work;
  version running_version;
  hardfork_version hardfork_version_vote;
  time_point_sec hardfork_time_vote;
};

struct signed_block_api_obj : public signed_block {
  signed_block_api_obj(const signed_block &block) : signed_block(block) {
     block_id = id();
     signing_key = signee();
     transaction_ids.reserve(transactions.size());
     for (const signed_transaction &tx : transactions)
        transaction_ids.push_back(tx.id());
  }

  signed_block_api_obj() {}

  block_id_type block_id;
  public_key_type signing_key;
  vector<transaction_id_type> transaction_ids;
};

struct dynamic_global_property_api_obj : public dynamic_global_property_object {
  dynamic_global_property_api_obj(const dynamic_global_property_object &gpo, const chain::database &db) :
     dynamic_global_property_object(gpo) {
     if (db.has_index<witness::reserve_ratio_index>()) {
        const auto &r = db.find(witness::reserve_ratio_id_type());

        if (BOOST_LIKELY(r != nullptr)) {
           current_reserve_ratio = r->current_reserve_ratio;
           average_block_size = r->average_block_size;
           max_virtual_bandwidth = r->max_virtual_bandwidth;
        }
     }
  }

  dynamic_global_property_api_obj(const dynamic_global_property_object &gpo) :
     dynamic_global_property_object(gpo) {}

  dynamic_global_property_api_obj() {}

  uint32_t current_reserve_ratio = 0;
  uint64_t average_block_size = 0;
  uint128_t max_virtual_bandwidth = 0;
};


//////////////////
struct applied_operation {
  applied_operation();

  applied_operation(const wls::chain::operation_object &op_obj);

  applied_operation(const wls::plugins::block_history::block_history_applied_operation &obj);

  wls::protocol::transaction_id_type trx_id;
  uint32_t block = 0;
  uint32_t trx_in_block = 0;
  uint16_t op_in_trx = 0;
  uint64_t virtual_op = 0;
  fc::time_point_sec timestamp;
  wls::protocol::operation op;
};


struct discussion_index {
  string category;            /// category by which everything is filtered
  vector<string> trending;    /// trending posts over the last 24 hours
  vector<string> payout;      /// pending posts by payout
  vector<string> payout_comments; /// pending comments by payout
  vector<string> trending30;  /// pending lifetime payout
  vector<string> created;     /// creation date
  vector<string> responses;   /// creation date
  vector<string> updated;     /// creation date
  vector<string> active;      /// last update or reply
  vector<string> votes;       /// last update or reply
  vector<string> cashout;     /// last update or reply
  vector<string> maturing;    /// about to be paid out
  vector<string> best;        /// total lifetime payout
  vector<string> hot;         /// total lifetime payout
};

struct tag_index {
  vector<string> trending; /// pending payouts
};

struct vote_state {
  string voter;
  uint64_t weight = 0;
  int64_t rshares = 0;
  int16_t percent = 0;
  share_type reputation = 0;
  time_point_sec time;
};

struct account_vote {
  string authorperm;
  uint64_t weight = 0;
  int64_t rshares = 0;
  int16_t percent = 0;
  time_point_sec time;
};

struct comment_tip {
  string tipper;
  asset amount; // in WLS
};

struct discussion : public comment_api_obj {
  discussion(const comment_object &o) : comment_api_obj(o) {}

  discussion() {}

  string url; /// /category/@rootauthor/root_permlink#author/permlink
  string root_title;
  asset pending_payout_value; ///< wls
  asset total_pending_payout_value; ///< wls including replies
  vector<vote_state> active_votes;
  vector<comment_tip> comment_tips;
  vector<string> replies; ///< author/slug mapping
  share_type author_reputation = 0;
  uint32_t body_length = 0;
  vector<account_name_type> reblogged_by;
  optional<account_name_type> first_reblogged_by;
  optional<time_point_sec> first_reblogged_on;
};

/**
*  Convert's vesting shares
*/
struct extended_account : public account_api_obj {
  extended_account() {}

  extended_account(const account_object &a, const database &db) : account_api_obj(a, db) {}

  asset vesting_balance; /// convert vesting_shares to vesting WLS (WHALESTAKE)
  share_type reputation = 0;
  map<uint64_t, applied_operation> transfer_history; /// transfer to/from vesting
  map<uint64_t, applied_operation> post_history;
  map<uint64_t, applied_operation> vote_history;
  map<uint64_t, applied_operation> other_history;
  set<string> witness_votes;
  vector<pair<string, uint32_t>> tags_usage;
  vector<pair<account_name_type, uint32_t>> guest_bloggers;

  set<pod_name_type> pods;                 /// list of pods that this account is member of

  optional<vector<string>> comments;       /// permlinks for this user
  optional<vector<string>> blog;           /// blog posts for this user
  optional<vector<string>> feed;           /// feed posts for this user
  optional<vector<string>> recent_replies; /// blog posts for this user
  optional<vector<string>> recommended;    /// posts recommened for this user
};

/**
*  This struct is designed
*/
struct state {
  string current_route;
  dynamic_global_property_api_obj props;
  tag_index tag_idx;

  /**
   * "" is the global discussion index
   */
  map<string, discussion_index> discussion_idx;
  map<string, tag_api_obj> tags;

  /**
   *  map from account/slug to full nested discussion
   */
  map<string, discussion> content;
  map<string, extended_account> accounts;

  /**
   * The list of miners who are queued to produce work
   */
  map<string, witness_api_obj> witnesses;
  witness_schedule_api_obj witness_schedule;
  string error;
};
// clang-format on

}  // namespace database_api
}  // namespace plugins
}  // namespace wls

FC_REFLECT(wls::plugins::database_api::health_check, (status)(message))
FC_REFLECT(wls::plugins::database_api::pod_api_obj, (id)(name)(created)(fee)(json_metadata)(allow_join)(member_count)(json_metadata_creator))
FC_REFLECT(wls::plugins::database_api::comment_api_obj,
    (id)(author)(permlink)(category)(parent_author)(parent_permlink)(title)(body)(json_metadata)(last_update)(created)(active)(last_payout)(depth)(children)(
        net_rshares)(abs_rshares)(vote_rshares)(children_abs_rshares)(cashout_time)(max_cashout_time)(total_vote_weight)(reward_weight)(total_payout_value)(
        curator_payout_value)(author_rewards)(net_votes)(root_comment)(max_accepted_payout)(allow_replies)(allow_votes)(allow_curation_rewards)(beneficiaries)
        (pod)(allow_friends))
FC_REFLECT(wls::plugins::database_api::account_api_obj,
           (id)(name)(owner)(active)(posting)(memo_key)(json_metadata)(proxy)(last_owner_update)(last_account_update)(created)(recovery_account)(comment_count)(
               lifetime_vote_count)(post_count)(voting_power)(last_vote_time)(balance)(reward_steem_balance)(reward_vesting_balance)(reward_vesting_steem)(
               vesting_shares)(vesting_withdraw_rate)(next_vesting_withdrawal)(withdrawn)(to_withdraw)(withdraw_routes)(curation_rewards)(posting_rewards)(
               proxied_vsf_votes)(witnesses_voted_for)(average_bandwidth)(lifetime_bandwidth)(last_bandwidth_update)(last_post)(last_root_post)
               (daily_bandwidth)(last_daily_bandwidth_update)
               (vr_snaphost_next_cycle)(vr_snaphost_shares)(vr_snaphost_cycle)(vr_claimed_cycle)(vr_claimed_amount)
               (is_pod))
FC_REFLECT(wls::plugins::database_api::tag_api_obj, (name)(total_payouts)(net_votes)(top_posts)(comments)(trending))
FC_REFLECT(wls::plugins::database_api::witness_api_obj,
           (id)(owner)(created)(url)(votes)(virtual_last_update)(virtual_position)(virtual_scheduled_time)(total_missed)(last_aslot)(last_confirmed_block_num)(
               signing_key)(props)(last_work)(running_version)(hardfork_version_vote)(hardfork_time_vote))
FC_REFLECT_DERIVED(wls::plugins::database_api::signed_block_api_obj, (wls::protocol::signed_block), (block_id)(signing_key)(transaction_ids))
FC_REFLECT_DERIVED(wls::plugins::database_api::dynamic_global_property_api_obj, (wls::chain::dynamic_global_property_object),
                   (current_reserve_ratio)(average_block_size)(max_virtual_bandwidth))
FC_REFLECT_DERIVED(wls::plugins::database_api::extended_account, (wls::plugins::database_api::account_api_obj),
                   (vesting_balance)(reputation)(transfer_history)(post_history)(vote_history)(other_history)(witness_votes)(tags_usage)(guest_bloggers)(pods)(
                       comments)(feed)(blog)(recent_replies)(recommended))
FC_REFLECT(wls::plugins::database_api::vote_state, (voter)(weight)(rshares)(percent)(reputation)(time));
FC_REFLECT(wls::plugins::database_api::account_vote, (authorperm)(weight)(rshares)(percent)(time));
FC_REFLECT(wls::plugins::database_api::comment_tip, (tipper)(amount));
FC_REFLECT(wls::plugins::database_api::discussion_index,
           (category)(trending)(payout)(payout_comments)(trending30)(updated)(created)(responses)(active)(votes)(maturing)(best)(hot)(cashout))
FC_REFLECT(wls::plugins::database_api::tag_index, (trending))
FC_REFLECT_DERIVED(wls::plugins::database_api::discussion, (wls::plugins::database_api::comment_api_obj),
                   (url)(root_title)(pending_payout_value)(total_pending_payout_value)(active_votes)(comment_tips)(replies)(author_reputation)(body_length)(reblogged_by)(
                       first_reblogged_by)(first_reblogged_on))
FC_REFLECT(wls::plugins::database_api::state, (current_route)(props)(tag_idx)(tags)(content)(accounts)(witnesses)(discussion_idx)(witness_schedule)(error))
FC_REFLECT(wls::plugins::database_api::applied_operation, (trx_id)(block)(trx_in_block)(op_in_trx)(virtual_op)(timestamp)(op))
FC_REFLECT(wls::plugins::database_api::discussion_query,
           (tag)(filter_tags)(select_tags)(select_authors)(truncate_body)(start_author)(start_permlink)(parent_author)(parent_permlink)(limit));
FC_REFLECT(wls::plugins::database_api::scheduled_hardfork, (hf_version)(live_time))
FC_REFLECT_ENUM(wls::plugins::database_api::withdraw_route_type, (incoming)(outgoing)(all));
FC_REFLECT(wls::plugins::database_api::withdraw_route, (from_account)(to_account)(percent)(auto_vest))
