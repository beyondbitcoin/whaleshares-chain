//#include <wls/chain/wls_object_types.hpp>
#include <wls/plugins/database_api/database_api_objects.hpp>
#include <wls/chain/history_object.hpp>

namespace wls {
namespace plugins {
namespace database_api {

applied_operation::applied_operation() {}

applied_operation::applied_operation(const operation_object &op_obj)
    : trx_id(op_obj.trx_id),
      block(op_obj.block),
      trx_in_block(op_obj.trx_in_block),
      op_in_trx(op_obj.op_in_trx),
      virtual_op(op_obj.virtual_op),
      timestamp(op_obj.timestamp) {
  // fc::raw::unpack( op_obj.serialized_op, op );     // g++ refuses to compile this as ambiguous
  op = fc::raw::unpack<operation>(op_obj.serialized_op);
}

applied_operation::applied_operation(const wls::plugins::block_history::block_history_applied_operation& obj)
    : trx_id(obj.trx_id),
      block(obj.block),
      trx_in_block(obj.trx_in_block),
      op_in_trx(obj.op_in_trx),
      virtual_op(obj.virtual_op),
      timestamp(obj.timestamp),
      op(obj.op) {
}

}  // namespace database_api
}  // namespace plugins
}  // namespace wls