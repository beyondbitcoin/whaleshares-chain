#include <wls/plugins/account_by_key_api/account_by_key_api_plugin.hpp>
#include <wls/plugins/account_by_key_api/account_by_key_api.hpp>
#include <wls/plugins/account_by_key/account_by_key_objects.hpp>

namespace wls {
namespace plugins {
namespace account_by_key {

namespace detail {

class account_by_key_api_impl {
 public:
  account_by_key_api_impl() : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()) {}

  /**
   * vector< vector< account_name_type > > get_key_references( vector< public_key_type > keys )const;
   */
  get_key_references_return get_key_references(const get_key_references_args &args) const;

  chain::database &_db;
};

get_key_references_return account_by_key_api_impl::get_key_references(const get_key_references_args &args) const {
  FC_ASSERT(args.size() == 1, "Expected 1 argument, was ${n}", ("n", args.size()));
  vector<public_key_type> keys = args[0].as<vector<public_key_type> >();

  vector<vector<wls::protocol::account_name_type> > final_result;
  final_result.reserve(keys.size());

  const auto &key_idx = _db.get_index<account_by_key::key_lookup_index>().indices().get<account_by_key::by_key>();

  for (auto &key : keys) {
    std::vector<wls::protocol::account_name_type> result;
    auto lookup_itr = key_idx.lower_bound(key);

    while (lookup_itr != key_idx.end() && lookup_itr->key == key) {
      result.push_back(lookup_itr->account);
      ++lookup_itr;
    }

    final_result.emplace_back(std::move(result));
  }

  return final_result;
}

}  // namespace detail

account_by_key_api::account_by_key_api() : my(new detail::account_by_key_api_impl()) { JSON_RPC_REGISTER_API(WLS_ACCOUNT_BY_KEY_API_PLUGIN_NAME); }

account_by_key_api::~account_by_key_api() {}

DEFINE_READ_APIS(account_by_key_api, (get_key_references))

}  // namespace account_by_key
}  // namespace plugins
}  // namespace wls
