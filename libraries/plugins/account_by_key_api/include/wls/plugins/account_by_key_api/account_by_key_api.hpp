#pragma once

#include <wls/plugins/json_rpc/utility.hpp>

#include <wls/protocol/types.hpp>

#include <fc/optional.hpp>
#include <fc/variant.hpp>
#include <fc/vector.hpp>

namespace wls {
namespace plugins {
namespace account_by_key {

namespace detail {
class account_by_key_api_impl;
}

typedef vector<variant> get_key_references_args;
typedef vector<vector<wls::protocol::account_name_type> > get_key_references_return;

class account_by_key_api {
 public:
  account_by_key_api();

  ~account_by_key_api();

  DECLARE_API((get_key_references))

 private:
  std::unique_ptr<detail::account_by_key_api_impl> my;
};

}  // namespace account_by_key
}  // namespace plugins
}  // namespace wls
