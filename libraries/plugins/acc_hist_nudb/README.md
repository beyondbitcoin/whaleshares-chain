# acc_hist_nudb

Account History using NuDB is a light-delayed-node (no consensus, and need a trust full-node to work) 
and optimized for SSD, low RAM for large history log activities (TB).


The idea is use NuDB to store the log activities on disk and chainbase to track the head (last activity record of each account) on RAM.

RAM is low as:
- it needs a little (eg., 128 bytes) for each account, so ~128MB for 1M accounts.
- insert needs small buffer (~16MB)
- fetching data uses no RAM for cache


Change `config.ini` to run account history service node:

```
lightnode = 1
plugin = chain p2p json_rpc webserver acc_hist_nudb account_history_api
```