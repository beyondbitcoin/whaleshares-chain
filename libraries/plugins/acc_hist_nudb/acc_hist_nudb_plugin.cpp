#include <wls/plugins/acc_hist_nudb/acc_hist_nudb_plugin.hpp>
#include <wls/plugins/acc_hist_nudb/acc_hist_nudb_objects.hpp>
#include <wls/plugins/acc_hist_nudb/nudbstore_database.hpp>
#include <wls/plugins/acc_hist_nudb/remote_node_api.hpp>
#include <wls/plugins/chain/chain_plugin.hpp>
#include <wls/plugins/database_api/database_api_objects.hpp>
#include <wls/chain/database.hpp>
#include <wls/chain/index.hpp>
#include <wls/chain/util/impacted.hpp>
#include <wls/protocol/config.hpp>
#include <wls/utilities/plugin_utilities.hpp>

#include <fc/rpc/http_api.hpp>
#include <fc/smart_ref_impl.hpp>
#include <fc/thread/thread.hpp>

#include <boost/algorithm/string.hpp>

namespace wls {
namespace plugins {
namespace acc_hist_nudb {

using namespace wls::protocol;

namespace detail {

class acc_hist_nudb_plugin_impl {
 public:
  acc_hist_nudb_plugin_impl(acc_hist_nudb_plugin &_plugin)
      : _db(appbase::app().get_plugin<wls::plugins::chain::chain_plugin>().db()), _self(_plugin) {
  }

  virtual ~acc_hist_nudb_plugin_impl() {}

  const acc_hist_nudb_state_object& get_acc_hist_nudb_state_object();

  const acc_hist_nudb_head_object* get_acc_hist_nudb_head_object(const account_name_type &account);

  /**
   * high level insert which appends action to log and updates head state
   * @param account
   * @param item
   */
  void insert(const account_name_type &account, const acc_hist_object_nudb &item);

  void mainloop();

  string trusted_node;
  uint32_t max_blocks = 10; // limit n blocks in get_ops_in_blocks

  fc::api<remote_database_api> _remote_database_api;
  shared_ptr<fc::rpc::http_api_connection> http_api_con;
  fc::thread mainloop_thread;

  database &_db;
  acc_hist_nudb_plugin &_self;
  nudbstore_database _nudbstore_database;

  std::atomic_bool running = ATOMIC_VAR_INIT(true);
};

void acc_hist_nudb_plugin_impl::mainloop() {
  ilog("acc_hist_nudb_plugin_impl::mainloop");

  while( running.load() ) {
    try {
      bool is_error = false;

      try {
        const auto& cstate = get_acc_hist_nudb_state_object();

        if (cstate.processed_block_number < cstate.last_irreversible_block_num) {
          uint32_t block_num = cstate.processed_block_number + 1;
          ilog("processing block ${block_num}/${last_irreversible_block_num}",
               ("block_num", block_num)("last_irreversible_block_num", cstate.last_irreversible_block_num));

          if ((max_blocks > 0) && (cstate.last_irreversible_block_num - block_num > max_blocks)) {
            // batch mode - fetch 10 blocks at a time
            auto ops_in_blocks = _remote_database_api->get_ops_in_blocks(block_num, max_blocks, true);

            // process operations
            for (auto& applied_op : ops_in_blocks) {
              if (!running.load()) break;

              flat_set<account_name_type> impacted;
              wls::app::operation_get_impacted_accounts(applied_op.op, impacted);

              for (auto& impacted_acc : impacted) {
                if (!running.load()) break;

                this->insert(impacted_acc, applied_op);
              }
            }

            // if it reach here then block processed OK, update state to last processed block
            _db.with_write_lock([&]() {
              uint32_t processed_block_number = block_num + max_blocks - 1;
              _db.modify(cstate, [&](acc_hist_nudb_state_object& state) { state.processed_block_number = processed_block_number; });
              _db.commit(processed_block_number);
            });
          } else {
            // normal mode, fetch single block at a time
            auto ops_in_block = _remote_database_api->get_ops_in_block(block_num, true);

            // process operations
            for (auto& applied_op : ops_in_block) {
              if (!running.load()) break;

              flat_set<account_name_type> impacted;
              wls::app::operation_get_impacted_accounts(applied_op.op, impacted);

              for (auto& impacted_acc : impacted) {
                if (!running.load()) break;

                this->insert(impacted_acc, applied_op);
              }
            }

            // if it reach here then block processed OK, update state to last processed block
            _db.with_write_lock([&]() {
              _db.modify(cstate, [&](acc_hist_nudb_state_object& state) { state.processed_block_number = block_num; });
              _db.commit(block_num);
            });
          }

          fc::usleep(fc::milliseconds(10));
        } else {
          const auto dpo = _remote_database_api->get_dynamic_global_properties();
          _db.with_write_lock([&]() {
            _db.modify(cstate, [&](acc_hist_nudb_state_object& state) {
              state.head_block_number = dpo.head_block_number;
              state.last_irreversible_block_num = dpo.last_irreversible_block_num;
            });
          });
          fc::usleep(fc::seconds(5));
        }
      } catch(...) {
        is_error = true;
      }

      if (is_error) {
        ilog("exception, wait 5s to retry");
        fc::usleep(fc::seconds(5));
      }
    } catch(...) {
      // do nothing
    }
  }

  ilog("acc_hist_nudb_plugin_impl::mainloop END");
}

const acc_hist_nudb_state_object& acc_hist_nudb_plugin_impl::get_acc_hist_nudb_state_object() {
  try {
    return _db.get<acc_hist_nudb_state_object>();
  }
  FC_CAPTURE_AND_RETHROW()
}

const acc_hist_nudb_head_object* acc_hist_nudb_plugin_impl::get_acc_hist_nudb_head_object(const account_name_type& account) {
  try {
    return _db.find<acc_hist_nudb_head_object, by_account>(account);
  }
  FC_CAPTURE_AND_RETHROW((account))
}

void acc_hist_nudb_plugin_impl::insert(const account_name_type& account, const acc_hist_object_nudb& item) {
  auto acc_hist_head_obj = this->get_acc_hist_nudb_head_object(account);
  if (acc_hist_head_obj == nullptr) {
    _db.with_write_lock([&]() {
      acc_hist_head_obj =
          &_db.create<acc_hist_nudb_head_object>([&](acc_hist_nudb_head_object& new_acc_hist_head_obj) { new_acc_hist_head_obj.account = account; });
    });
  }

  // insert
  acc_hist_object_nudb_key new_key{account, acc_hist_head_obj->sequence};
  ulog("new_key: ${new_key}, item: ${item}", ("new_key", new_key)("item", item.trx_id));
  error_code ec;
  _nudbstore_database.insert(new_key, item, ec);
  if (ec) {
    if ((ec == nudb::error::key_exists) && (ec.category() == nudb::nudb_category())) {
      // ignore key_exists
    } else {
      FC_ASSERT(true, "${msg}", ("msg", ec.message()));
    }
  }

  // update last sequence to account head state
  _db.with_write_lock([&]() {
    _db.modify(*acc_hist_head_obj, [&](acc_hist_nudb_head_object& ahh) { ahh.sequence++; });
  });
}

}  // end namespace detail

acc_hist_nudb_plugin::acc_hist_nudb_plugin() {}

acc_hist_nudb_plugin::~acc_hist_nudb_plugin() {}

void acc_hist_nudb_plugin::set_program_options(options_description &cli, options_description &cfg) {
  // clang-format off
  cfg.add_options(
  )(
      "trusted-node", bpo::value<string>()->default_value("https://pubrpc.whaleshares.io"), "RPC endpoint of a trusted validating node."
  );
  // clang-format on
  cfg.add(cli);
}

void acc_hist_nudb_plugin::plugin_initialize(const boost::program_options::variables_map &options) {
  ilog("Initializing acc_hist_nudb plugin");

  my = std::make_unique<detail::acc_hist_nudb_plugin_impl>(*this);

  try {
    if (options.count("trusted-node")) {
      my->trusted_node = options.at("trusted-node").as<string>();
    }
    if (options.count("max-blocks")) {
      my->max_blocks = options.at("max-blocks").as<uint32_t>();
    }

    add_plugin_index<acc_hist_nudb_state_index>(my->_db);
    add_plugin_index<acc_hist_nudb_head_index>(my->_db);
  } FC_CAPTURE_AND_RETHROW()
}

void acc_hist_nudb_plugin::plugin_startup() {
  ilog("acc_hist_nudb plugin: plugin_startup()");

  try {
    {
      // create acc_hist_nudb_state_object if not exist
      auto state_ptr = my->_db.find(oid<acc_hist_nudb_state_object>());
      if (state_ptr == nullptr) {
        my->_db.with_write_lock([&]() {
          my->_db.create<acc_hist_nudb_state_object>([&](acc_hist_nudb_state_object& state) {
            state.head_block_number = 0;
            state.last_irreversible_block_num = 0;
          });
        });
      }
    }

    my->_nudbstore_database.open();
    my->http_api_con = std::make_shared<fc::rpc::http_api_connection>(my->trusted_node);
    my->_remote_database_api = my->http_api_con->get_remote_api<remote_database_api>(0, "database_api");
    my->mainloop_thread.async([this] {
      my->mainloop();
    });

  } FC_CAPTURE_AND_RETHROW()
}

void acc_hist_nudb_plugin::plugin_shutdown() {
  try {
    ilog("acc_hist_nudb_plugin::plugin_shutdown");

    my->running.store(false);
    my->mainloop_thread.quit();
    my->_nudbstore_database.close();

    ilog("acc_hist_nudb_plugin::plugin_shutdown OK");
  } FC_CAPTURE_AND_RETHROW()
}

nudbstore_database& acc_hist_nudb_plugin::get_nudbstore_database() {
  return my->_nudbstore_database;
}

}  // namespace acc_hist_nudb
}  // namespace plugins
}  // namespace wls
