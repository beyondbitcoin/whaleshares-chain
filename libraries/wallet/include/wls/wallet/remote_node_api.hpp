#pragma once

#include <fc/api.hpp>

#include <wls/plugins/database_api/database_api_objects.hpp>
#include <wls/plugins/network_broadcast_api/network_broadcast_api.hpp>

namespace wls {
namespace wallet {

using fc::optional;
using fc::variant;
using std::vector;

using namespace chain;
using namespace plugins;
using namespace plugins::database_api;
//      using namespace wls::api;

/**
 * This is a dummy class exists only to provide method signature information to fc::api, not to execute calls.
 * Class is used by wallet to send formatted API calls to database_api plugin on remote node.
 */
struct remote_database_api {
  dynamic_global_property_api_obj get_dynamic_global_properties();

  witness_schedule_api_obj get_witness_schedule();

  hardfork_version get_hardfork_version();

  chain_properties get_chain_properties();

  reward_fund_api_obj get_reward_fund(string name);

  vector<extended_account> get_accounts(vector<string> names);

  fc::optional<witness_api_obj> get_witness_by_account(string account_name);

  vector<applied_operation> get_ops_in_block(uint32_t block_num, bool only_virtual = true);

  set<string> lookup_accounts(string lower_bound_name, uint32_t limit);

  vector<account_name_type> get_active_witnesses();

  set<account_name_type> lookup_witness_accounts(string lower_bound_name, uint32_t limit);

  vector<withdraw_route> get_withdraw_routes(string account, withdraw_route_type type = outgoing);

  optional<signed_block_api_obj> get_block(uint32_t block_num);
};

/**
 * This is a dummy class exists only to provide method signature information to fc::api, not to execute calls.
 * Class is used by wallet to send formatted API calls to network_broadcast_api plugin on remote node.
 */
struct remote_network_broadcast_api {
  void broadcast_transaction(signed_transaction);

  network_broadcast_api::broadcast_transaction_synchronous_return broadcast_transaction_synchronous(signed_transaction);

  //         void broadcast_block(signed_block);
};

/**
 * This is a dummy class for account_by_key_api
 */
struct remote_account_by_key_api {
  vector<vector<account_name_type> > get_key_references(vector<public_key_type> keys);
};

}  // namespace wallet
}  // namespace wls
// clang-format off

FC_API(wls::wallet::remote_database_api,
  (get_dynamic_global_properties)
  (get_witness_schedule)
  (get_hardfork_version)
  (get_chain_properties)
  (get_reward_fund)
  (get_accounts)
  (get_witness_by_account)
  (get_ops_in_block)
  (lookup_accounts)
  (get_active_witnesses)
  (lookup_witness_accounts)
  (get_withdraw_routes)
  (get_block)
)
// clang-format on
FC_API(wls::wallet::remote_account_by_key_api, (get_key_references))

FC_API(wls::wallet::remote_network_broadcast_api, (broadcast_transaction)(broadcast_transaction_synchronous))