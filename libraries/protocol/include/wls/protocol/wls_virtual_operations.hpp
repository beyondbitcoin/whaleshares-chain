#pragma once
#include <wls/protocol/base.hpp>
#include <wls/protocol/block_header.hpp>
#include <wls/protocol/asset.hpp>

#include <fc/utf8.hpp>

namespace wls {
namespace protocol {

using pod_name_type = account_name_type;

struct author_reward_operation : public virtual_operation {
  author_reward_operation(){}
  author_reward_operation( const account_name_type& a, const string& p, const asset& st, const asset& v )
     :author(a), permlink(p), steem_payout(st), vesting_payout(v){}

  account_name_type author;
  string            permlink;
  asset             steem_payout;
  asset             vesting_payout;
};


struct curation_reward_operation : public virtual_operation
{
  curation_reward_operation(){}
  curation_reward_operation( const string& c, const asset& r, const string& a, const string& p )
     :curator(c), reward(r), comment_author(a), comment_permlink(p) {}

  account_name_type curator;
  asset             reward;
  account_name_type comment_author;
  string            comment_permlink;
};


struct comment_reward_operation : public virtual_operation
{
  comment_reward_operation(){}
  comment_reward_operation( const account_name_type& a, const string& pl, const asset& p )
     :author(a), permlink(pl), payout(p){}

  account_name_type author;
  string            permlink;
  asset             payout;
};

struct fill_vesting_withdraw_operation : public virtual_operation
{
  fill_vesting_withdraw_operation(){}
  fill_vesting_withdraw_operation( const string& f, const string& t, const asset& w, const asset& d )
     :from_account(f), to_account(t), withdrawn(w), deposited(d) {}

  account_name_type from_account;
  account_name_type to_account;
  asset             withdrawn;
  asset             deposited;
};


struct shutdown_witness_operation : public virtual_operation {
  shutdown_witness_operation(){}
  shutdown_witness_operation( const string& o ):owner(o) {}

  account_name_type owner;
};

struct witness_block_missed_operation : public virtual_operation {
  witness_block_missed_operation() {}
  witness_block_missed_operation(const string& o, const uint32_t& n, const fc::time_point_sec& t)
  : owner(o), block_num(n), timestamp(t)  {}

  account_name_type owner;
  uint32_t block_num;
  fc::time_point_sec timestamp;
};

struct hardfork_operation : public virtual_operation
{
  hardfork_operation() {}
  hardfork_operation( uint32_t hf_id ) : hardfork_id( hf_id ) {}

  uint32_t         hardfork_id = 0;
};

struct comment_payout_update_operation : public virtual_operation
{
  comment_payout_update_operation() {}
  comment_payout_update_operation( const account_name_type& a, const string& p ) : author( a ), permlink( p ) {}

  account_name_type author;
  string            permlink;
};

struct comment_benefactor_reward_operation : public virtual_operation
{
  comment_benefactor_reward_operation() {}
  comment_benefactor_reward_operation( const account_name_type& b, const account_name_type& a, const string& p, const asset& r )
     : benefactor( b ), author( a ), permlink( p ), reward( r ) {}

  account_name_type benefactor;
  account_name_type author;
  string            permlink;
  asset             reward;
};

struct producer_reward_operation : public virtual_operation
{
  producer_reward_operation(){}
  producer_reward_operation( const string& p, const asset& v ) : producer( p ), vesting_shares( v ) {}

  account_name_type producer;
  asset             vesting_shares;

};

struct devfund_operation : public virtual_operation
{
  devfund_operation(){}
  devfund_operation( const string& a, const asset& v ) : account( a ), reward( v ) {}

  account_name_type account;
  asset             reward;
};

/**
 * transfer join fee to pod escrow (WLS_ESCROW_POD_NAME)
 */
struct pod_escrow_transfer_virtual_action {
  pod_escrow_transfer_virtual_action(){}
  pod_escrow_transfer_virtual_action(const string& a, const string& c, const asset& v) : account(a), pod(c), amount(v) {}

  account_name_type account;            /// account transfers to escrow
  pod_name_type pod;                    /// join fee of this pod
  asset amount = asset(0, WLS_SYMBOL);  /// must be matched to pod join_fee when validating
};

/**
 * release join fee to pod owner or user
 */
struct pod_escrow_release_virtual_action {
  pod_escrow_release_virtual_action(){}
  pod_escrow_release_virtual_action(const string& a, const string& c, const asset& v) : account(a), pod(c), amount(v) {}

  account_name_type account;            /// who receives the fee (user in case rejected or pod owner in case accepted)
  pod_name_type pod;                    /// join fee of this pod
  asset amount = asset(0, WLS_SYMBOL);  /// must be matched to pod join_fee when validating
};

// not sure if creating an alias works as pod_escrow_release_virtual_action is exactly the same to pod_escrow_transfer_virtual_action
//typedef pod_escrow_transfer_virtual_action pod_escrow_release_virtual_action;

typedef static_variant<
  /* 0 */ pod_escrow_transfer_virtual_action,
  /* 1 */ pod_escrow_release_virtual_action
> pod_virtual_action;

/**
 * all virtual operations for pod defined in pod_virtual_action
 */
struct pod_virtual_operation : public virtual_operation {
  pod_virtual_operation() {}
  pod_virtual_operation(const pod_virtual_action& a) : action(a) {}

  pod_virtual_action action;  /// what
};


//-----------------------------------------
//-- for HTLC

/**
 * virtual op to assist with notifying related parties
 */
struct htlc_redeemed_virtual_action {
  htlc_redeemed_virtual_action() {}
  htlc_redeemed_virtual_action(htlc_ref_id_type htlc_id, account_name_type from, account_name_type to, account_name_type redeemer, asset amount)
      : htlc_id(htlc_id), from(from), to(to), redeemer(redeemer), amount(amount) {}

  htlc_ref_id_type htlc_id;
  account_name_type from;
  account_name_type to;
  account_name_type redeemer;
  asset amount;
};

struct htlc_refund_virtual_action {
  htlc_refund_virtual_action() {}
  htlc_refund_virtual_action(const htlc_ref_id_type& htlc_id, const account_name_type& to) : htlc_id(htlc_id), to(to) {}

  htlc_ref_id_type htlc_id;
  account_name_type to;
};

typedef static_variant<
  /* 0 */ htlc_redeemed_virtual_action,
  /* 1 */ htlc_refund_virtual_action
> htlc_virtual_action;

/**
 * all virtual operations for htlc defined in htlc_virtual_action
 */
struct htlc_virtual_operation : public virtual_operation {
  htlc_virtual_operation() {}
  htlc_virtual_operation(const htlc_virtual_action& a) : action(a) {}

  htlc_virtual_action action;  /// what
};

}  // namespace protocol
}  // namespace wls

FC_REFLECT( wls::protocol::author_reward_operation, (author)(permlink)(steem_payout)(vesting_payout) )
FC_REFLECT( wls::protocol::curation_reward_operation, (curator)(reward)(comment_author)(comment_permlink) )
FC_REFLECT( wls::protocol::comment_reward_operation, (author)(permlink)(payout) )
FC_REFLECT( wls::protocol::fill_vesting_withdraw_operation, (from_account)(to_account)(withdrawn)(deposited) )
FC_REFLECT( wls::protocol::shutdown_witness_operation, (owner) )
FC_REFLECT( wls::protocol::witness_block_missed_operation, (owner)(block_num)(timestamp) )
FC_REFLECT( wls::protocol::hardfork_operation, (hardfork_id) )
FC_REFLECT( wls::protocol::comment_payout_update_operation, (author)(permlink) )
FC_REFLECT( wls::protocol::comment_benefactor_reward_operation, (benefactor)(author)(permlink)(reward) )
FC_REFLECT( wls::protocol::producer_reward_operation, (producer)(vesting_shares) )
FC_REFLECT( wls::protocol::devfund_operation, (account)(reward) )
FC_REFLECT( wls::protocol::pod_escrow_transfer_virtual_action, (account)(pod)(amount) )
FC_REFLECT( wls::protocol::pod_escrow_release_virtual_action, (account)(pod)(amount) )
FC_REFLECT_TYPENAME(wls::protocol::pod_virtual_action)
FC_REFLECT( wls::protocol::pod_virtual_operation, (action) )
FC_REFLECT(wls::protocol::htlc_redeemed_virtual_action, (htlc_id)(from)(to)(redeemer)(amount))
FC_REFLECT(wls::protocol::htlc_refund_virtual_action, (htlc_id)(to))
FC_REFLECT_TYPENAME(wls::protocol::htlc_virtual_action)
FC_REFLECT( wls::protocol::htlc_virtual_operation, (action) )