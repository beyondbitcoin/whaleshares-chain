#include <wls/protocol/wls_operations.hpp>
#include <fc/io/json.hpp>

#include <locale>

namespace wls {
namespace protocol {

bool inline is_asset_type(asset asset, asset_symbol_type symbol) { return asset.symbol == symbol; }

void account_create_operation::validate() const {
  validate_account_name(new_account_name);
  FC_ASSERT(is_asset_type(fee, WLS_SYMBOL), "Account creation fee must be WLS");
  owner.validate();
  active.validate();

  if (json_metadata.size() > 0) {
    FC_ASSERT(fc::is_utf8(json_metadata), "JSON Metadata not formatted in UTF8");
    FC_ASSERT(fc::json::is_valid(json_metadata), "JSON Metadata not valid JSON");
  }
  FC_ASSERT(fee >= asset(0, WLS_SYMBOL), "Account creation fee cannot be negative");
}

void account_update_operation::validate() const {
  validate_account_name(account);
  /*if( owner )
     owner->validate();
  if( active )
     active->validate();
  if( posting )
     posting->validate();*/

  if (json_metadata.size() > 0) {
    FC_ASSERT(fc::is_utf8(json_metadata), "JSON Metadata not formatted in UTF8");
    FC_ASSERT(fc::json::is_valid(json_metadata), "JSON Metadata not valid JSON");
  }
}

void comment_operation::validate() const {
  FC_ASSERT(title.size() < 256, "Title larger than size limit");
  FC_ASSERT(fc::is_utf8(title), "Title not formatted in UTF8");
  FC_ASSERT(body.size() > 0, "Body is empty");
  FC_ASSERT(fc::is_utf8(body), "Body not formatted in UTF8");

  if (parent_author.size()) validate_account_name(parent_author);
  validate_account_name(author);
  validate_permlink(parent_permlink);
  validate_permlink(permlink);

  if (json_metadata.size() > 0) {
    FC_ASSERT(fc::json::is_valid(json_metadata), "JSON Metadata not valid JSON");
  }
}

struct comment_options_extension_validate_visitor {
  comment_options_extension_validate_visitor() {}

  typedef void result_type;

  void operator()(const comment_payout_beneficiaries& cpb) const { cpb.validate(); }
};

void comment_payout_beneficiaries::validate() const {
  uint32_t sum = 0;

  FC_ASSERT(beneficiaries.size(), "Must specify at least one beneficiary");
  // dont need this anymore
//  FC_ASSERT(beneficiaries.size() < 128, "Cannot specify more than 127 beneficiaries.");  // Require size serialization fits in one byte.
  FC_ASSERT(beneficiaries.size() <= 8, "Cannot specify more than 8 beneficiaries."); // moved from witness plugin

  validate_account_name(beneficiaries[0].account);
  FC_ASSERT(beneficiaries[0].weight <= WLS_100_PERCENT, "Cannot allocate more than 100% of rewards to one account");
  sum += beneficiaries[0].weight;
  FC_ASSERT(sum <= WLS_100_PERCENT, "Cannot allocate more than 100% of rewards to a comment");  // Have to check incrementally to avoid overflow

  for (size_t i = 1; i < beneficiaries.size(); i++) {
    validate_account_name(beneficiaries[i].account);
    FC_ASSERT(beneficiaries[i].weight <= WLS_100_PERCENT, "Cannot allocate more than 100% of rewards to one account");
    sum += beneficiaries[i].weight;
    FC_ASSERT(sum <= WLS_100_PERCENT, "Cannot allocate more than 100% of rewards to a comment");  // Have to check incrementally to avoid overflow
    FC_ASSERT(beneficiaries[i - 1] < beneficiaries[i], "Beneficiaries must be specified in sorted order (account ascending)");
  }
}

void comment_options_operation::validate() const {
  validate_account_name(author);
  FC_ASSERT((max_accepted_payout.symbol == WLS_SYMBOL), "Max accepted payout must be in WLS");
  FC_ASSERT(max_accepted_payout.amount.value >= 0, "Cannot accept less than 0 payout");
  validate_permlink(permlink);
  for (auto& e : extensions) e.visit(comment_options_extension_validate_visitor());
}

void delete_comment_operation::validate() const {
  validate_permlink(permlink);
  validate_account_name(author);
}

void vote_operation::validate() const {
  validate_account_name(voter);
  validate_account_name(author);
  FC_ASSERT(abs(weight) <= WLS_100_PERCENT, "Weight is not valid percentage");
  validate_permlink(permlink);
}

void transfer_operation::validate() const {
  try {
    validate_account_name(from);
    validate_account_name(to);
    FC_ASSERT(is_asset_type(amount, WLS_SYMBOL), "Amount must be WLS");
//    FC_ASSERT(amount.symbol != VESTS_SYMBOL, "transferring of WHALESTAKE is not allowed.");
    FC_ASSERT(amount.amount > 0, "Cannot transfer a negative amount (aka: stealing)");
    FC_ASSERT(memo.size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
    FC_ASSERT(fc::is_utf8(memo), "Memo is not UTF8");
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void transfer_to_vesting_operation::validate() const {
  validate_account_name(from);
  FC_ASSERT(is_asset_type(amount, WLS_SYMBOL), "Amount must be WLS");
  if (to != WLS_ROOT_POST_PARENT) validate_account_name(to);
  FC_ASSERT(amount > asset(0, WLS_SYMBOL), "Must transfer a nonzero amount");
}

void withdraw_vesting_operation::validate() const {
  validate_account_name(account);
  FC_ASSERT(is_asset_type(vesting_shares, VESTS_SYMBOL), "Amount must be VESTS");
}

void set_withdraw_vesting_route_operation::validate() const {
  validate_account_name(from_account);
  validate_account_name(to_account);
  FC_ASSERT(0 <= percent && percent <= WLS_100_PERCENT, "Percent must be valid wls percent");
}

void witness_update_operation::validate() const {
  validate_account_name(owner);
  FC_ASSERT(url.size() > 0, "URL size must be greater than 0");
  FC_ASSERT(fc::is_utf8(url), "URL is not valid UTF8");
  FC_ASSERT(fee >= asset(0, WLS_SYMBOL), "Fee cannot be negative");
  props.validate();
}

void account_witness_vote_operation::validate() const {
  validate_account_name(account);
  validate_account_name(witness);
}

void account_witness_proxy_operation::validate() const {
  validate_account_name(account);
  if (proxy.size()) validate_account_name(proxy);
  FC_ASSERT(proxy != account, "Cannot proxy to self");
}

void custom_operation::validate() const {
  /// required auth accounts are the ones whose bandwidth is consumed
  FC_ASSERT(required_auths.size() > 0, "at least on account must be specified");
}

void custom_json_operation::validate() const {
  /// required auth accounts are the ones whose bandwidth is consumed
  FC_ASSERT((required_auths.size() + required_posting_auths.size()) > 0, "at least on account must be specified");
  FC_ASSERT(id.size() <= 32, "id is too long");
  FC_ASSERT(fc::is_utf8(json), "JSON Metadata not formatted in UTF8");
  FC_ASSERT(fc::json::is_valid(json), "JSON Metadata not valid JSON");
}

void custom_binary_operation::validate() const {
  /// required auth accounts are the ones whose bandwidth is consumed
  FC_ASSERT((required_owner_auths.size() + required_active_auths.size() + required_posting_auths.size()) > 0, "at least on account must be specified");
  FC_ASSERT(id.size() <= 32, "id is too long");
  for (const auto& a : required_auths) a.validate();
}

void claim_reward_balance_operation::validate() const {
  validate_account_name(account);
  FC_ASSERT(is_asset_type(reward_steem, WLS_SYMBOL), "Reward WLS must be WLS");
  FC_ASSERT(is_asset_type(reward_vests, VESTS_SYMBOL), "Reward VESTS must be VESTS");
  FC_ASSERT(reward_steem.amount >= 0, "Cannot claim a negative amount");
  FC_ASSERT(reward_vests.amount >= 0, "Cannot claim a negative amount");
  FC_ASSERT(reward_steem.amount > 0 || reward_vests.amount > 0, "Must claim something.");
}

struct pod_action_validate_visitor {
  pod_action_validate_visitor() {}

  typedef void result_type;

  void operator()(const pod_action_join_request& ca) const { ca.validate(); }
  void operator()(const pod_action_cancel_join_request& ca) const { ca.validate(); }
  void operator()(const pod_action_accept_join_request& ca) const { ca.validate(); }
  void operator()(const pod_action_reject_join_request& ca) const { ca.validate(); }
  void operator()(const pod_action_leave& ca) const { ca.validate(); }
  void operator()(const pod_action_kick& ca) const { ca.validate(); }
  void operator()(const pod_action_update& ca) const { ca.validate(); }
};

void pod_action_operation::validate() const {
  validate_account_name(account);
  validate_account_name(pod);

  action.visit(pod_action_validate_visitor());
}

void pod_action_join_request::validate() const {
  try {
    FC_ASSERT(is_asset_type(join_fee, WLS_SYMBOL), "pod join_fee must be WLS");
    FC_ASSERT(join_fee >= asset(0, WLS_SYMBOL), "pod join_fee must not be negative");

    FC_ASSERT(is_asset_type(fee, WLS_SYMBOL), "pod join request fee must be WLS");
    FC_ASSERT(fee >= asset(0, WLS_SYMBOL), "pod join request fee must not be negative");

    FC_ASSERT(memo.size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
    FC_ASSERT(fc::is_utf8(memo), "Memo is not UTF8");
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void pod_action_cancel_join_request::validate() const {}

void pod_action_accept_join_request::validate() const {
  validate_account_name(account);
}

void pod_action_reject_join_request::validate() const {
  try {
    validate_account_name(account);

    FC_ASSERT(memo.size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
    FC_ASSERT(fc::is_utf8(memo), "Memo is not UTF8");
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void pod_action_leave::validate() const {}

void pod_action_kick::validate() const {
  try {
    validate_account_name(account);

    FC_ASSERT(memo.size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
    FC_ASSERT(fc::is_utf8(memo), "Memo is not UTF8");
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void pod_action_update::validate() const {
  try {
    bool has_something = false;

    if (join_fee) {
      FC_ASSERT(*join_fee >= asset(0, WLS_SYMBOL), "pod join_fee must not be negative");
      has_something = true;
    }

    if (json_metadata) {
      if (json_metadata->size() > 0) {
        FC_ASSERT(fc::is_utf8(*json_metadata), "JSON Metadata not formatted in UTF8");
        FC_ASSERT(fc::json::is_valid(*json_metadata), "JSON Metadata not valid JSON");
      }

      has_something = true;
    }

    if (allow_join) {
      has_something = true;
    }

    FC_ASSERT(has_something, "nothing to update");
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

struct friend_action_validate_visitor {
  friend_action_validate_visitor() {}

  typedef void result_type;

  void operator()(const friend_action_send_request& fa) const { fa.validate(); }
  void operator()(const friend_action_cancel_request& fa) const { fa.validate(); }
  void operator()(const friend_action_accept_request& fa) const { fa.validate(); }
  void operator()(const friend_action_reject_request& fa) const { fa.validate(); }
  void operator()(const friend_action_unfriend& fa) const { fa.validate(); }
};

void friend_action_operation::validate() const {
  validate_account_name(account);
  validate_account_name(another);

  action.visit(friend_action_validate_visitor());
}

void friend_action_send_request::validate() const {
  try {
    FC_ASSERT(memo.size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
    FC_ASSERT(fc::is_utf8(memo), "Memo is not UTF8");
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void friend_action_cancel_request::validate() const {}
void friend_action_accept_request::validate() const {}
void friend_action_reject_request::validate() const {}
void friend_action_unfriend::validate() const {}

void account_action_create_pod::validate() const {
  FC_ASSERT(is_asset_type(fee, WLS_SYMBOL), "pod creation fee must be WLS");
  FC_ASSERT(fee >= asset(0, WLS_SYMBOL), "pod creation fee must not be negative");

  if (join_fee) {
    FC_ASSERT(is_asset_type(*join_fee, WLS_SYMBOL), "pod join_fee must be WLS");
    FC_ASSERT(*join_fee >= asset(0, WLS_SYMBOL), "pod join_fee must not be negative");
  }

  if (json_metadata) {
    if (json_metadata->size() > 0) {
      FC_ASSERT(fc::is_utf8(*json_metadata), "JSON Metadata not formatted in UTF8");
      FC_ASSERT(fc::json::is_valid(*json_metadata), "JSON Metadata not valid JSON");
    }
  }
}

void account_action_transfer_to_tip::validate() const {
  FC_ASSERT((amount.symbol == WLS_SYMBOL), "Amount must be in WLS");
  FC_ASSERT(amount.amount.value > 0, "Amount must be > 0");
}

void htlc_sha256::validate() const {}

void account_action_htlc_create::validate() const {
  try {
    // fee
    FC_ASSERT((fee.symbol == WLS_SYMBOL), "fee must be in WLS");
    FC_ASSERT(fee.amount.value >= 0, "fee must be >= 0");

    // reward
    FC_ASSERT((reward.symbol == WLS_SYMBOL), "reward must be in WLS");
    FC_ASSERT(reward.amount.value >= 0, "reward must be >= 0");

    // to
    validate_account_name(to);

    // amount
    FC_ASSERT((amount.symbol == WLS_SYMBOL), "Amount must be in WLS");
    FC_ASSERT(amount.amount.value > 0, "Amount must be > 0");

    // preimage_hash

    // preimage_size
    FC_ASSERT( preimage_size >= 0, "HTLC preimage length must be >= 0" );
    FC_ASSERT( preimage_size < 2048, "HTLC preimage length exceeds allowed length (2048)" );

    // expiration
    FC_ASSERT( expiration > 0, "HTLC Timeout must be > 0" );
    FC_ASSERT( expiration <= (60*60*24*30), "HTLC Timeout exceeds allowed length (30 days)" ); // 30 days

    // memo
    if (memo) {
      FC_ASSERT(memo->size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
      if (memo->size() > 0) FC_ASSERT(fc::is_utf8(*memo), "memo not formatted in UTF8");
    }
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void account_action_htlc_update::validate() const {
  try {
    // fee
    FC_ASSERT((fee.symbol == WLS_SYMBOL), "fee must be in WLS");
    FC_ASSERT(fee.amount.value >= 0, "fee must be >= 0");

    // htlc_id

    // seconds
    FC_ASSERT( seconds > 0, "added HTLC Timeout add must be > 0" );
    FC_ASSERT( seconds <= (60*60*24*30), "HTLC Timeout exceeds allowed length ( 30 days)" ); // 30 days

    // reward
    FC_ASSERT((reward.symbol == WLS_SYMBOL), "reward must be in WLS");
    FC_ASSERT(reward.amount.value >= 0, "reward must be >= 0");

    // memo
    if (memo) {
      FC_ASSERT(memo->size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
      if (memo->size() > 0) FC_ASSERT(fc::is_utf8(*memo), "memo not formatted in UTF8");
    }
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void account_action_htlc_redeem::validate() const {
  try {
    // fee
    FC_ASSERT((fee.symbol == WLS_SYMBOL), "fee must be in WLS");
    FC_ASSERT(fee.amount.value >= 0, "fee must be >= 0");

    // htlc_id

    // preimage
    FC_ASSERT( preimage.size() > 0, "HTLC preimage length must be > 0" );
    FC_ASSERT( preimage.size() <= 2048, "HTLC preimage length exceeds allowed length (2048)" );

    // memo
    if (memo) {
      FC_ASSERT(memo->size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
      if (memo->size() > 0) FC_ASSERT(fc::is_utf8(*memo), "memo not formatted in UTF8");
    }
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

struct account_action_validate_visitor {
  account_action_validate_visitor() {}

  typedef void result_type;

  void operator()(const account_action_create_pod& ac) const { ac.validate(); }
  void operator()(const account_action_transfer_to_tip& ac) const { ac.validate(); }
  void operator()(const account_action_htlc_create& ac) const { ac.validate(); }
  void operator()(const account_action_htlc_update& ac) const { ac.validate(); }
  void operator()(const account_action_htlc_redeem& ac) const { ac.validate(); }
};

void account_action_operation::validate() const {
  validate_account_name(account);

  action.visit(account_action_validate_visitor());
}

void social_action_comment_create::validate() const {
  FC_ASSERT(title.size() < 256, "Title larger than size limit");
  FC_ASSERT(fc::is_utf8(title), "Title not formatted in UTF8");
  FC_ASSERT(body.size() > 0, "Body is empty");
  FC_ASSERT(body.size() < 24 * 1024, "Body larger than size limit");
  FC_ASSERT(fc::is_utf8(body), "Body not formatted in UTF8");

  if (parent_author.size()) validate_account_name(parent_author);

  validate_permlink(parent_permlink);
  validate_permlink(permlink);

  if (json_metadata.size() > 0) {
    FC_ASSERT(json_metadata.size() < 24 * 1024, "JSON Metadata larger than size limit");
    FC_ASSERT(fc::json::is_valid(json_metadata), "JSON Metadata not valid JSON");
    FC_ASSERT(fc::is_utf8(json_metadata), "JSON Metadata must be UTF-8");
  }

  if (pod) {
    validate_account_name(*pod);
    FC_ASSERT(parent_author.size() == 0, "Not allowed to set pod on non-root comment");
  }

  if (max_accepted_payout) {
    FC_ASSERT((max_accepted_payout->symbol == WLS_SYMBOL), "Max accepted payout must be in WLS");
    FC_ASSERT(max_accepted_payout->amount.value >= 0, "Cannot accept less than 0 payout");
  }

  if (allow_friends) {
    FC_ASSERT(!(*allow_friends && pod), "option allow_friends allowed when no pod specified only");
    FC_ASSERT(parent_author.size() == 0, "option allow_friends allowed on root comment only");
  }
}

void social_action_comment_update::validate() const {
  validate_permlink(permlink);

  uint32_t total_size = 0;

  if (title) {
    FC_ASSERT(title->size() < 256, "Title larger than size limit");
    FC_ASSERT(fc::is_utf8(*title), "Title not formatted in UTF8");
    total_size += title->size();
  }

  if (body) {
    FC_ASSERT(body->size() > 0, "Body is empty");
    FC_ASSERT(body->size() < 24 * 1024, "Body larger than size limit");
    FC_ASSERT(fc::is_utf8(*body), "Body not formatted in UTF8");
    total_size += body->size();
  }

  if (json_metadata) {
    if (json_metadata->size() > 0) {
      FC_ASSERT(json_metadata->size() < 24 * 1024, "JSON Metadata larger than size limit");
      FC_ASSERT(fc::json::is_valid(*json_metadata), "JSON Metadata not valid JSON");
      FC_ASSERT(fc::is_utf8(*json_metadata), "JSON Metadata must be UTF-8");
    }
    total_size += json_metadata->size();
  }

  FC_ASSERT(total_size, "Cannot update comment because nothing appears to be changing.");
}

void social_action_comment_delete::validate() const {
  validate_permlink(permlink);
}

void social_action_claim_vesting_reward::validate() const {
  try {
    FC_ASSERT((amount.symbol == WLS_SYMBOL), "Amount must be in WLS");
    FC_ASSERT(amount.amount.value > 0, "Amount must be > 0");

    // to
    if (to) {
      if (*to != WLS_ROOT_POST_PARENT) validate_account_name(*to);
    }

    // memo
    if (memo) {
      FC_ASSERT(memo->size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
      if (memo->size() > 0) FC_ASSERT(fc::is_utf8(*memo), "JSON Metadata not formatted in UTF8");
    }
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void social_action_claim_vesting_reward_tip::validate() const {
  try {
    FC_ASSERT((amount.symbol == WLS_SYMBOL), "Amount must be in WLS");
    FC_ASSERT(amount.amount.value > 0, "Amount must be > 0");
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void social_action_user_tip::validate() const {
  try {
    FC_ASSERT((amount.symbol == WLS_SYMBOL), "Amount must be in WLS");
    FC_ASSERT(amount.amount.value > 0, "Amount must be > 0");

    // to
    if (to) {
      if (*to != WLS_ROOT_POST_PARENT) validate_account_name(*to);
    }

    // memo
    if (memo) {
      FC_ASSERT(memo->size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
      if (memo->size() > 0) FC_ASSERT(fc::is_utf8(*memo), "JSON Metadata not formatted in UTF8");
    }
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

void social_action_comment_tip::validate() const {
  try {
    validate_account_name(author);
    validate_permlink(permlink);
    FC_ASSERT((amount.symbol == WLS_SYMBOL), "Amount must be in WLS");
    FC_ASSERT(amount.amount.value > 0, "Amount must be > 0");

    // memo
    if (memo) {
      FC_ASSERT(memo->size() < WLS_MAX_MEMO_SIZE, "Memo is too large");
      if (memo->size() > 0) FC_ASSERT(fc::is_utf8(*memo), "JSON Metadata not formatted in UTF8");
    }
  }
  FC_CAPTURE_AND_RETHROW((*this))
}

struct social_action_validate_visitor {
  social_action_validate_visitor() {}

  typedef void result_type;

  template<typename T>
  void operator()( const T& v )const { v.validate(); }

//  void operator()(const social_action_comment_create& sa) const { sa.validate(); }
//  void operator()(const social_action_comment_update& sa) const { sa.validate(); }
//  void operator()(const social_action_comment_delete& sa) const { sa.validate(); }
//  void operator()(const social_action_claim_vesting_reward& sa) const { sa.validate(); }
};

void social_action_operation::validate() const {
  validate_account_name(account);

  action.visit(social_action_validate_visitor());
}

}  // namespace protocol
}  // namespace wls
